<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|https://www.coinw.com/3hds/it/index.html
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [App\Http\Controllers\LandingPageController::class, 'index'])->name('landingPage');
Route::get('/tentangKami', [App\Http\Controllers\LandingPageController::class, 'about'])->name('tentangKami');
Route::get('/surveiUser', [App\Http\Controllers\SurveiUserController::class, 'surveiUser'])->name('survei');
Route::get('/survei/{id_survei}', [App\Http\Controllers\SurveiUserController::class, 'surveiIsi'])->name('surveiIsi');
Route::post('/PostJawabanSurvei', [\App\Http\Controllers\SurveiUserController::class,'postJawabanSurvei'])->name('surveiUserPost');
Route::get('/ThanksForm', [App\Http\Controllers\SurveiUserController::class,'ThanksForm'])->name('ThanksForm');

Route::group(["middleware"=> "guest"], function () {
    Route::get('/login', [App\Http\Controllers\LoginController::class, 'index'])->name('login');
    Route::post('/login', [App\Http\Controllers\LoginController::class, 'authenticate'])->name('cheklogin');
});

Route::group(['middleware'=> 'auth'], function () {
    Route::group(['middleware'=> 'user.akses:3'], function () {
        Route::get('/dashboardAdmin', [\App\Http\Controllers\DashboardController::class, 'admin'])->name('admin');
        Route::resource('/SuratMasuk', \App\Http\Controllers\SuratMasukController::class);
        Route::resource('/SuratKeluar', \App\Http\Controllers\SuratKeluarController::class);
        Route::resource('/Bidang', \App\Http\Controllers\BidangController::class);
        Route::resource('/UserSenapelan', \App\Http\Controllers\UserSenapelanController::class);
        Route::resource('/SurveiAdmin', \App\Http\Controllers\SurveiAdminController::class);
        Route::get('/DetailSurvei/{id}', [\App\Http\Controllers\SurveiAdminController::class, 'detailSurvei'])->name('detailSurvei');
        Route::post('/InsertPertanyaanPost', [\App\Http\Controllers\SurveiAdminController::class, 'insertPertanyaan']);
        Route::post('/HapusPertanyaan/{id_pertanyaan}/{id}', [\App\Http\Controllers\SurveiAdminController::class, 'hapusPertanyaan'])->name('hapusPertanyaan');
        Route::match(['put', 'patch'], '/EditPertanyaan/{id_pertanyaan}/{id}', [\App\Http\Controllers\SurveiAdminController::class, 'editPertanyaan'])->name('editPertanyaan');
        Route::get('/InsertPertanyaan/{id}', [\App\Http\Controllers\SurveiAdminController::class, 'getInsertPertanyaan'])->name('insertPertanyaan');
        Route::get('/MasyarakatSurvei/{id_survei}', [\App\Http\Controllers\SurveiAdminController::class, 'masyarakatSurvei'])->name('masyarakatSurvei');
        Route::get('/DataJawabanSurvei/{id_pertanyaan}/{id_survei}', [\App\Http\Controllers\SurveiAdminController::class,'dataJawabanSurvei'])->name('dataJawabanSurvei');
    });
    Route::group(['middleware'=> 'user.akses:1'], function () {
        Route::get('/dashboardCamat', [\App\Http\Controllers\DashboardController::class, 'camat'])->name('camat');
        Route::get('/SuratMasukCamat', [App\Http\Controllers\SuratMasukCamatController::class, 'index'])->name('SuratMasukCamat');
        Route::post('/SuratMasukCamat/{id}', [App\Http\Controllers\SuratMasukCamatController::class, 'deposisi'])->name('deposisi');
        Route::get('/SuratKeluarCamat', [App\Http\Controllers\SuratKeluarCamatController::class, 'index'])->name('SuratKeluarCamat');
        Route::get('/SurveiCamat', [App\Http\Controllers\SurveiCamatController::class, 'index'])->name('SurveiMasyarakat');
        Route::get('/DetailSurveiCamat/{id}', [\App\Http\Controllers\SurveiCamatController::class, 'detailSurvei'])->name('detailSurveiCamat');
        Route::get('/DataJawabanSurveiCamat/{id_pertanyaan}/{id_survei}', [\App\Http\Controllers\SurveiCamatController::class,'dataJawabanSurvei'])->name('dataJawabanSurveiCamat');
    });
    Route::group(['middleware'=> 'user.akses'], function () {
        Route::get('/dashboardPegawai', [\App\Http\Controllers\DashboardController::class, 'pegawai'])->name('pegawai');
        Route::get('/SuratMasukPegawai', [\App\Http\Controllers\SuratMasukPegawaiController::class, 'index'])->name('SuratMasukPegawai');
        Route::get('/SuratKeluarPegawai', [\App\Http\Controllers\SuratKeluarPegawaiController::class, 'index'])->name('SuratKeluarPegawai');
    });
    Route::post('/logout', [App\Http\Controllers\LoginController::class, 'logout']);
});
