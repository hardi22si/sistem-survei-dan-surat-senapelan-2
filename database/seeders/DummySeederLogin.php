<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DummySeederLogin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userData = [
            [
                "name"=> "Susan",
                "email"=> "susan@gmail.com",
                "email_verified_at"=> now(),
                "password"=> bcrypt("123456"),
                "hak_akses"=> "Yes",
                "kode"=> "4",
                "remember_token" => Str::random(10),
            ],
            [
                "name"=> "Dono",
                "email"=> "dono@gmail.com",
                "email_verified_at"=> now(),
                "password"=> bcrypt("123456"),
                "hak_akses"=> "No",
                "kode"=> "5",
                "remember_token" => Str::random(10),
            ],
            [
                "name"=> "Udin",
                "email"=> "udin@gmail.com",
                "email_verified_at"=> now(),
                "password"=> bcrypt("123456"),
                "hak_akses"=> "Yes",
                "kode"=> "4",
                "remember_token" => Str::random(10),
            ]
        ];
        foreach( $userData as $key => $value ) {
            User ::create($value);
        }
    }
}
