![Gambar](image/cover.jpg?raw=true)

## BAB I PENDAHULUAN

### 1.1 Tujuan
> Dokumen Software Requirement Specification (SRS) merupakan dokumen spesifikasi perangkat lunak untuk membangun "Sistem Pengelolaan Surat dan Sistem Survei Pelayanan Masyarakat di Kec. Senapelan". Dokumen ini dibangun untuk memudahkan pemerintahan kecamatan Senapelan untuk menerima surat masuk dan mengarsipkan surat keluar secara mudah dan cepat diterima informasi suratnya oleh Kepala Bidang serta dengan ada nya sistem Survei pelayanan Masyarakat akan dapat membantu Kecamatan Senapelan dalam menilai kinerja dan pelayanan yang diberikan oleh kecamatan Senapelan untuk penduduk di sekitar nya. Sehingga dokumen ini dapat dijadikan acuan teknis untuk membangun perangkat lunak "Sistem Pengelolaan Surat dan Sistem Survei Pelayanan Masyarakat di Kec. Senapelan".

### 1.2 Lingkup
Sistem Pengelolaan Surat dan Sistem Survei Pelayanan Masyarakat di Kec. Senapelan merupakan sistem yang berguna untuk membantu dan  memudahkan  petugas kecamatan dalam penerimaan surat masuk dan mengeluarkan surat keluar di kecamatan senapelan secara cepat dan mudah. Serta dalam menilai tingkat Pelayanan Petugas kecamatan senapelan dalam melayani masyarakat dan pengaduan masyarakat terkait pelayanan nya.

### 1.3 Akronim, singkatan, definisi
| Istilah  | Definisi |
| ------ | ------ |
|    Login    |    Digunakan untuk mengakses aplikasi    |
|    Register    |    Digunakan untuk membuat hak akses aplikasi    |
|    Logout    |   Digunakan untuk keluar dari hak akses Sistem     |
|    Software Requirement Specification    |    Perangkat lunak yang akan dibuat dan sebagai menjembatani komunikasi pembuat dengan pengguna    |
|    Use Case    |    Situasi dimana sistem anda dipergunakan untuk memenuhi satu atau lebih kebutuhan pemakaian anda    |
|    Entity Relationship Diagram    |    Beberapa tabel logika penempatan data secara terstruktur    |

### 1.4 Refrensi

##### Refrensi Pengembangan Perangkat Lunak Ini:
- http://jurnalmahasiswa.com/index.php/Jurihum/article/view/328
- https://ejournal.bsi.ac.id/ejurnal/index.php/khatulistiwa/article/view/5738
- https://ejournal.cip.or.id/index.php/Inseds/article/view/466

### 1.5 Overview
Bab selanjutnya yaitu menjelaskan sistem yang diterapkan pada Website. Menjelaskan gambaran umum dari Website, sistem interface Website dan alur sistemnya. Bab terakhir menjelaskan tentang setiap fungsi yang digunakan secara teknisnya. Pada bab 2 dan 3 merupakan deskripsi dari Website yang akan diterapkan pada Website yang dibuat.

## BAB II GAMBARAN UMUM
Pada zaman era globalisasi perkembangan teknologi begitu sangat pesat, salah satunya ialah perkembangan teknologi di bidang software engineering dimana software engineering dapat digunakan dalam kehidupan sehari - hari .dalam studi kasus Proyek ini kami menganalisis kebutuhan suatu kecamatan di daerah Senapelan.kasus yang kami peroleh untuk membuat Sistem Pengelolaan Surat dan Sistem Survei Pelayanan Masyarakat di Kec. Senapelan. Sehingga sistem ini dapat memudahkan admin dalam menerima surat masuk dan pengarsipan surat keluar secara cepat dan mudah untuk menyebarkan informasi surat keberbagai kepala bidang yang ada di kecamatan Senapelan Serta Camat dapat mengetahui Tingkat Kinerja Pelayanan setiap bidang kecamatan Senapelan dengan adanya Sistem Survei yang di isi oleh masyarakat. Software berbasis website yang kami buat ini dimana seorang kasubag dan seorang IT adalah admin yang bertanggung jawab mengelola data survei dan surat,dan camat hanya metindak lanjuti surat kearah bidang yang mana dan mengarsipkan surat masuk dan keluar serta melihat hasil Survei dari sistem. Sistem yang kami buat didalamnya terdapat 5 bidang atau seksi untuk menerima surat masuk dan keluar serta bidang yang akan di survei yaitu:

1. Camat
2. Kasubag Umum
3. Sekretaris Camat
4. Kepala Seksi Terdiri Dari:
    Kepala Seksi Trantib
    Kepala Seksi Pemerintahan
    Kepala Seksi PPM
    Kepala Seksi Kesos
    Kepala Seksi Paten

Berikut akan kami jelaskan sistem software kami,
Kasubag Umum fungsi utama yaitu:

- Tambah surat masuk dan keluar
- Hapus surat masuk dan keluar
- Update surat masuk dan keluar
- Lihat surat masuk dan keluar
- Tambah Form dan data pertanyaan Survei
- Hapus Form dan data pertanyaan Survei
- Update Form dan data pertanyaan Survei
- Lihat Form dan data pertanyaan Survei
- Kirim surat masuk ke camat

Camat fungsi utama yaitu:

- Lihat surat masuk dan keluar
- Deposisi Ditindaklanjuti atau diarsipkan surat masuk ke salah satu bidang
- Melihat Data form survei 

Kepala Seksi fungsi utama yaitu:

- Lihat Data Surat masuk dan keluar

Masyarakat fungsi utama yaitu:

- Mengisi Data Form Survei

### 2.1 Perspektif produk

Sistem Pengelolaan Surat dan Sistem Survei Pelayanan Masyarakat di Kec. Senapelan adalah sistem penerimaan surat masuk dan keluar serta sistem Survei yang di aplikasikan pada website. Pengolahan data dikelola oleh kasubag umum dan IT dari kecamatan senapelan sebagai admin dan camat hanya metindak lanjuti surat dan mengarsipkan surat serta melihat data laporan Survei pada website.

##### 2.1.1 Antarmuka sistem
![Gambar](image/AntarMukaSistem.png?raw=true)

##### 2.1.2 Antarmuka pengguna

###### Halaman Login (All User)
![Gambar](image/AntarmukaPengguna1.png?raw=true)

###### Halaman Pengelolaan Data Surat Masuk (Admin)
![Gambar](image/AntarmukaPengguna2.png?raw=true)

###### Halaman Pengelolaan Data Surat Keluar (Admin)
![Gambar](image/AntarmukaPengguna3.png?raw=true)

###### Halaman Pengelolaan Data Anggota (Admin)
![Gambar](image/AntarmukaPengguna4.png?raw=true)

###### Halaman Deposisi Camat
![Gambar](image/AntarmukaPengguna7.png?raw=true)

###### Halaman View Surat Masuk Anggota Bidang
![Gambar](image/AntarmukaPengguna5.png?raw=true)

###### Halaman View Surat Keluar Anggota Bidang
![Gambar](image/AntarmukaPengguna6.png?raw=true)

###### Halaman Data Survey (Admin)
![Gambar](image/AntarmukaPengguna8.png?raw=true)

###### Halaman Pertanyaan Survey (Admin)
![Gambar](image/AntarmukaPengguna9.png?raw=true)

##### Link Figma
https://www.figma.com/file/pSvzikrY034zAxMoFIi7Yt/Desain-Rancangan-Sistem-Pengelolaan-Data-Surat-Masuk-Dan-Keluar-Kecamatan-Senapelan?type=design&node-id=0%3A1&mode=design&t=lOEDHQuTf0xuzgRA-1

##### 2.1.3 Antarmuka perangkat keras

![Gambar](image/antarmukaperangkatkeras.png?raw=true)

##### 2.1.4 Antarmuka perangkat lunak

Tidak Ada

##### 2.1.5 Antarmuka Komunikasi

Antarmuka komunikasi yang digunakan untuk mengoperasikan Perangkat Lunak Sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan  antara lain :

- Kabel Lan UTP RJ45
- Modem
- Wifi

##### 2.1.6 Batasan memori
Tidak ada

##### 2.1.7 Operasi-operasi
| Operasi  | Fungsi |
| ------ | ------ |
|    Login    |   Digunakan untuk mengakses sistem     |
|    Logout    |   Digunakan untuk keluar dari hak akses Sistem     |
|    Input    |    Digunakan untuk memasukkan data-data    |
|    Kembali    |    Digunakan untuk kembali ke halaman sebelumnya    |
|    Deposisi    |    Digunakan untuk menentukan posisi data    |
|    Hapus    |    Digunakan untuk menghapus data    |
|    Edit    |    Digunakan untuk mengubah data    |
|    View    |    Digunakan untuk menampilkan data    |
|    Simpan    |    Digunakan untuk menyimpan data    |
|    Kirim    |    Digunakan untuk mengirim data ketujuan tertentu   |
|    Status    |    Digunakan untuk keterangan data   |

##### 2.1.8 Kebutuhan adaptasi

Tidak ada

### 2.2 Spesifikasi kebutuhan fungsional

![Gambar](image/usecasediagram.png?raw=true)

##### 2.2.1 Kasubag umum dan kasi seksi Login
Use Case Kasubag umum, Camat dan Kasi seksi Login

Diagram:
![Gambar](image/Login.png?raw=true)

Deskripsi Singkat Kasubag Umum, Camat, Kasi Seksi dapat melakukan login dengan memasukkan email dan password. 

Deskripsi langkah - langkah 
1. Kasubag Umum dan kasi seksi dapat melakukan login dengan email dan password 
2. Sistem melakukan validasi login 
3. Jika sukses maka sistem akan menampilkan dashboard 
4. Jika gagal maka akan menampilkan peringatan bahwa password atau email salah

Xref : 2.2.1 Kasubag umum, Camat dan kasi seksi Login


##### 2.2.2 Kasubag umum mengelola data surat masuk 
Use Case Diagram Kasubag umum dpaat mengelola data surat masuk  

Diagram: 
![Gambar](image/KasubagUmumSuratMasuk.png?raw=true)

Deskripsi singkat kasubag umum dapat melakukan mengelola data surat masuk dan sistem menyimpan data pada database.

Deskripsi Langkah - langkah : 
1. Kasubag umum  menerima data surat masuk 
2. Kasubag umum   mengupload file surat masuk
3. Sistem akan menyimpan file data surat masuk 
4. Kasubag umum dapat menghapus, mengedit data surat masuk

Xref : 2.2.2 Kasubag umum mengelola data surat masuk 

##### 2.2.3 Kasubag umum mengelola data surat keluar 

Use Case Diagram Kasubag umum dpaat mengelola data surat keluar  

Diagram: 
![Gambar](image/KasubagUmumSuratKeluar.png?raw=true)

Deskripsi singkat kasubag umum dapat melakukan mengelola data surat keluar dan sistem menyimpan data pada database.

Deskripsi Langkah - langkah : 
1. Kasubag umum  menerima data surat keluar 
2. Kasubag umum   mengupload file surat keluar
3. Sistem akan menyimpan file data surat keluar 
4. Kasubag umum dapat menghapus, mengedit data surat keluar

Xref : 2.2.3 Kasubag umum mengelola data surat keluar

##### 2.2.4 Camat dan Kasi seksi Melihat data surat masuk
Use Case Diagram Camat dan Kasi seksi dapat melihat surat masuk

Diagram: 
![Gambar](image/CamatDanKasiSeksiMelihatDataSuratMasuk.png?raw=true)

Deskripsi Singkat Camat dan Kasi seksi dapat melihat data Surat masuk ibaratkan sebagai User.

Deskripsi langkah - langkah :
1. Camat dan Kasi seksi membuka halaman data surat masuk.
2. Sistem akan menampilkan data surat masuk sesuai proporsi level masing masing user.
3. Camat dan Kasi seksi Bisa melihat data surat yang masuk.

Xref : 2.2.4 Camat kasi seksi melihat data surat masuk

##### 2.2.5 Camat dan Kasi seksi melihat data surat keluar 
Use Case Diagram Camat dan Kasi seksi melihat data surat keluar

Diagram: 
![Gambar](image/CamatDanKasiSeksiMelihatDataSuratKeluar.png?raw=true)

Deskripsi singkat Camat dan Kasi seksi dapat melihat data surat keluar ibaratkan user. 

Deskripsi langkah - langkah : 

1. Camat dan Kasi seksi membuka halaman data surat keluar.
2. Sistem akan menampilkan data surat keluar sesuai proporsi level masing masing user.
3. Camat dan Kasi seksi Bisa melihat data surat keluar. 

Xref : 2.2.5 Camat dan Kasi seksi melihat data surat keluar 

##### 2.2.6 Kasubag umum mengelola data pegawai 
Use Case Diagram kasubag umum mengelola data pegawai.

Diagram:
 ![Gambar](image/KasubagUmumMengelolaDataPegawai.png?raw=true)

Deskripsi singkat kasubag umum dapat mengelola data pegawai. 

DDeskripsi langkah - langkah :
1. Kasubag umum input data pegawai.
2. Sistem akan menyimpan data pegawai.
3. Kasubag umum dapat menghapus, mengedit data pegawai.
4. Sistem akan menanggapi perubahan pada data pegawai.

Xref : 2.2.6 kasubag umum mengelola data pegawai

##### 2.2.7 Kasubag umum mengirim surat ke camat 
Use Case Diagram Kasubag umum mengisirim surat ke camat 

Diagram: 
![Gambar](image/KasubagUmumMengirimSuratMasuk.png?raw=true)

Deskripsi singkat Kasubag umum mengirim surat ke camat. 

Deskripsi langkah - langkah : 
1. Kasubag umum membuka data surat masuk
2. Sistem akan menampilkan data surat masuk
3. Kasubag umum chek data surat masuk
4. Kasubag umum kirim surat ke camat.
5. Sistem akan menanggapi permintaan dan memproses agar surat masuk ke halaman data surat masuk Camat
6. Jika tidak Approve maka oleh sistem surat masuk akan di Arsipkan

Xref : 2.2.7 Kasubag umum mengirim surat masuk ke camat 

#### 2.2.8 Kasubag umum mengelola data form 
Use Case diagram kasubag umum mengelola data form 

Diagram: 
![Gambar](image/KasubagUmumMengelolaDataForm.png?raw=true)

Deskripsi singkat kasubg umum dapat mengelola data form.
Deskripsi langkah-langkah : 
1. Kasubag umum input data form 
2. Sistem akan menyimpan inputan 
3. Kasubag umum dapat menghapus, mengedit dan mneg Update form
4. Sistem akan menanggapi perubahan pada data.

Xref : 2.2.8 Kasubag umum mengelola data form 

#### 2.2.9 Camat deposisi surat 
Use Case diagram camat deposisi surat 

 Diagram: 
 ![Gambar](image/CamatDeposisiSurat.png?raw=true)

Deskripsi singkat camat dapat mendeposisikan surat apakah di Approve atau tidak 
Deskripsi langkah - langkah : 
1. Camat Menerima Data Surat
2. Camat mendeposisi Surat masuk 

Xref : 2.2.9 camat mendeposisi surat 

#### 2.2.10 Camat melihat form survei 
Use Case diagram camat melihat form survei

Diagram: 
![Gambar](image/MelihatLaporanSurvei.png?raw=true)

Deskripsi singkat camat melihat form survei.
Deskripsi langkah-langkah :
1. Camat membuka halaman form survei 
2. Sistem akan menampilkan data form survei 
3. Camat bisa melihat data form survei 

Xref : 2.2.10 camat melihat form survei 

#### 2.2.11 masyarakat mengisi form survei 
Use Case diagram masyarakat mengisi form survei 

Diagram: 
![Gambar](image/MengisiFormSurvei.png?raw=true)

Deskripsi singkat masyarakat dapat mengisi form survei yang disediakan 
Deskripsi langkah-langkah :
1. Masyarakat membuka halaman survei 
2. Sistem menampilkan form survei 
3. Masyarakat mengisi survei yang telah disediakan dan dapat juga mengisi ulang survei 
4. Sistem menyimpan jawaban survei 

Xref : 2.2.11 masyarakat mengisi form survei 

### 2.3 Spesifikasi kebutuhan non-fungsional

Tabel Kebutuhan Non-Fungsional

| No | Deskripsi |
| ------ | ------ |
|    1    |    Semua sistem menggunakan Bahasa Indonesia    |
|    2    |    Perangkat Lunak dapat dipakai di semua platform OS(Admin, Camat, Sekretaris Camat, Kepala Seksi, Masyarakat)    |

### 2.4 Karakteristik Pengguna

Karakteristik pengguna dari perangkat lunak ini adalah pengguna harus melakukan Autentikasi untuk bisa mendapat hak akses ke sistem karena Sistem bersifat Private untuk Anggota didalam nya yang sudah terdaftar agar dapat berinteraksi dengan sistem.

### 2.5 Batasan-batasan

- Perangkat lunak web hanya dijalankan di windows (7, 8, 10, 11), Android dan IOS.

### 2.6 Asumsi

Maksimal penginputan id atau memasukkan kode pada aplikasi ini adalah 9999, lebih dari itu program akan muncul peringatan"Anda telah melebihi batas maksimum".

### 2.7 Kebutuhan Penyeimbang

Tidak ada

## BAB III Requirement specification

### 3.1 Persyaratan Antarmuka Eksternal

Salah satu cara mengakses Internal Website ini yaitu dengan cara login menggunakan username dan password. Kemudian sistem akan mencocokkan username dan password yang telah dimasukkan oleh petugas kecamatan. Setelah login berhasil petugas kecamatan dapat melihat surat masuk dan surat keluar melalui sistem sesuai role masing masing.

### 3.2 Functional Requirement

##### 3.2.1 Kasubag umum, Camat dan kasi seksi Login

|  |  |
|--|--|
| Nama Fungsi | Login |
| Xref | Bagian 2.2.1, Kasubag umum, Camat dan kasi seksi Login |
| Trigger | Membuka website sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan |
| Precondition | Halaman Login |
| Basic Path | 1. Kasubag Umum, Camat, dan kasi seksi dapat melakukan login dengan email dan password <br> 2. Sistem melakukan validasi login <br> 3. Jika sukses maka sistem akan menampilkan beranda <br> 4. Jika gagal maka akan menampilkan peringatan bahwa password atau email salah  |
| Alternative | Tidak ada |
| Post Condition | Kasubag umum, Camat, dan kasi seksi dapat login dan mengakses website sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan sesuai dengan masing masing role user |
| Exception Push | Nama dan password salah |

##### 3.2.2 Kasubag umum mengelola data surat masuk

|  |  |
|--|--|
| Nama Fungsi | Mengelola data surat masuk |
| Xref | Bagian 2.2.2 Kasubag umum mengelola data surat masuk |
| Trigger | Membuka website sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan,melakukan login. |
| Precondition | Halaman Data surat masuk |
| Basic Path | 1. Kasubag umum  menerima data surat masuk <br> 2. Kasubag umum   mengupload file surat masuk <br> 3. Sistem akan menyimpan file data surat masuk <br> 4. Kasubag umum dapat menghapus, mengedit data surat masuk |
| Alternative | Tidak ada |
| Post Condition | Kasubag umum dapat mengelola data surat |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.3 Kasubag umum mengelola data surat keluar

|  |  |
|--|--|
| Nama Fungsi | Mengelola data surat keluar |
| Xref | Bagian 2.2.3 Kasubag umum mengelola data surat keluar |
| Trigger | Membuka website sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan,melakukan login. |
| Precondition | Halaman Data surat keluar |
| Basic Path | 1. Kasubag umum  menerima data surat keluar <br> 2. Kasubag umum   mengupload file surat keluar <br> 3. Sistem akan menyimpan file data surat keluar <br> 4. Kasubag umum dapat menghapus, mengedit data surat keluar |
| Alternative | Tidak ada |
| Post Condition | Kasubag umum dapat mengelola data surat keluar |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.4 Camat dan Kasi seksi Melihat data surat masuk

|  |  |
|--|--|
| Nama Fungsi | Melihat data surat masuk |
| Xref | Bagian 2.2.4 Camat dan Kasi seksi Melihat data surat masuk |
| Trigger | Sudah melakukan login dan membuka data surat masuk. |
| Precondition | Halaman dashboard Data surat masuk |
| Basic Path | 1. Camat atau Kasi seksi membuka halaman data surat masuk. <br> 2.Sistem akan menampilkan data surat masuk atau keluar sesuai proporsi level masing masing user. <br> 3. Camat atau Kasi seksi Bisa melihat data surat yang masuk |
| Alternative | Tidak ada |
| Post Condition | Camat atau Kasi seksi dapat melihat data surat masuk |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.5 Camat dan Kasi seksi melihat data surat keluar

|  |  |
|--|--|
| Nama Fungsi | Melihat data surat keluar |
| Xref | Bagian 2.2.4 Camat dan Kasi seksi melihat data surat keluar|
| Trigger | Sudah melakukan login dan membuka data surat keluar |
| Precondition | Halaman dashboard Data surat keluar |
| Basic Path | 1. Camat atau Kasi seksi membuka halaman data surat keluar. <br> 2. Sistem akan menampilkan data surat keluar sesuai proporsi level masing masing user. <br> 3. Camat atau Kasi seksi Bisa melihat data surat keluar. |
| Alternative | Tidak ada |
| Post Condition | Camat atau Kasi seksi dapat melihat data surat keluar |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.6 Kasubag umum mengelola data pegawai

|  |  |
|--|--|
| Nama Fungsi | Kasubag umum mengelola data pegawai |
| Xref | Bagian 2.2.5 Kasubag umum mengelola data pegawai |
| Trigger | Membuka website sistem Pengelolaan Surat Masuk dan Surat keluar Kec. Senapelan,melakukan login |
| Precondition | Halaman mengelola data anggota |
| Basic Path | 1. kasubag umum input data anggota bidang <br>2. Sistem akan menyimpan data anggota bidang <br> 3. Kasubag umum dapat menghapus, mengedit data anggota bidang <br>4. Sistem akan menanggapi perubahan pada data. |
| Alternative | Tidak ada |
| Post Condition | Kasubag umum dapat mengelola data anggota |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.7 Kasubag umum mengirim surat ke camat

|  |  |
|--|--|
| Nama Fungsi | Kasubag umum mengirim surat ke Camat |
| Xref | Bagian 2.2.6 Kasubag umum mengirim surat ke camat |
| Trigger | Sudah melakukan login dan membuka data surat |
| Precondition | Halaman dashboard data surat |
| Basic Path | 1. Kasubag umum membuka data surat masuk <br>2. Sistem akan menampilkan data surat masuk  <br>3. Kasubag umum chek data surat masuk <br>4. Kasubag umum kirim surat ke camat.<br>5. Sistem akan menanggapi permintaan dan memproses agar surat masuk ke halaman data surat masuk Camat <br>6. jika tidak Approve maka oleh sistem surat masuk akan di Arsipkan|
| Alternative | Tidak ada |
| Post Condition | Kasubag umum dapat  mengirim surat ke Camat  |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.8 Kasubag umum mengelola data form

|  |  |
|--|--|
| Nama Fungsi | Kasubag umum mengelola data form  |
| Xref | 2.2.7 Kasubag umum mengelola data form |
| Trigger | Membuka website sistem Pengelolaan data form Kec. Senapelan,melakukan login  |
| Precondition | Halaman dashboard data form |
| Basic Path | 1. Kasubag umum input data form.<br>2. sistem akan menyimpan inputan <br>3.kasubag umum dapat menghapus, mengedit dan mneg Update form<br>4. sistem akan menanggapi perubahan pada data. |
| Alternative | Tidak ada |
| Post Condition | Kasubag umum dapat mengelola data form |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.9 Camat deposisi surat

|  |  |
|--|--|
| Nama Fungsi | Camat deposisi surat |
| Xref | 2.2.10 Camat deposisi surat |
| Trigger | Sudah melakukan login dan membuka form surat |
| Precondition | Halaman dashboard data form surat |
| Basic Path | 1. Sistem menampilkan halaman Dashboard surat <br>2. Camat mengecheck surat <br>3. Camat deposisikan surat<br>4.Camat klik button send yang telah diposisikan <br>5.Jika gagal maka akan menampilkan peringatan |
| Alternative | Tidak ada |
| Post Condition | Camat dapat mendeposisi surat |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.10 Camat melihat form survei

|  |  |
|--|--|
| Nama Fungsi | Camat melihat form survei  |
| Xref | 2.2.8 Camat melihat form survei |
| Trigger | Sudah melakukan login dan membuka la  |
| Precondition | Halaman dashboard form survei |
| Basic Path | 1. camat membuka halaman form survei <br>2. sistem akan menmapilkan data form survei.<br>3. camat bisa melihat data form survei |
| Alternative | Tidak ada |
| Post Condition | Camat dapat melihat form survei |
| Exception Push | Tidak ada koneksi karna data belum diinput |

##### 3.2.11 masyarakat mengisi form survei

|  |  |
|--|--|
| Nama Fungsi | masyarakat mengisi form survei |
| Xref | 2.2.11 masyarakat mengisi form survei |
| Trigger | Sudah melakukan login dan membuka form survei |
| Precondition | Halaman dashboard data form survei |
| Basic Path | 1. masyarakat membuka halaman survei <br>2. sistem menampilkan form survei <br>3.masyarakat mengisi survei yang telah disediakan dan dapat juga mengisi ulang survei <br> 4. sistem menyimpan jawaban survei |
| Alternative | Tidak ada |
| Post Condition | masyarakat dapat mengisi form survei |
| Exception Push | Tidak ada koneksi karna data belum diinput |

### 3.3 Struktur Detail Kebutuhan Non-fungsional

Struktur data logika pada sistem terdapat struktur Database yang dijelaskan menggunakan ERD.

##### 3.3.1 Logika Struktur Data
Entitiy Relationship Diagram
![Gambar](image/ERDSenapelan.png?raw=true)

#### Tabel users

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id    |    int    |    Nomor auto increment Id_user    |
|    email    |    varchar    |    Merupakan level dari user    |
|    password    |    varchar    |    Berisikan data password pengguna untuk verifikasi    |
|    nama    |    varchar    |    Berisikan nama dari pengguna untuk verifikasi    |

#### Tabel bidang

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    kode    |    int    |    Nomor dari masing masing bidang (NO INCREMENT)    |
|    bidang    |    varchar    |    berisikan bidang bidang yang di tempati oleh user    |
|    deskripsi    |    varchar    |    berisikan penjelasan tentang bidang    |

#### Tabel surat_masuk

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id_surat_masuk    |    int    |    Nomor auto increment id_surat_masuk    |
|    nama_surat_masuk    |    varchar    |    Merupakan nama dari surat masuk   |
|    tanggal_masuk    |    date    |    Merupakan tanggal surat yang masuk   |
|    asal_surat    |    text    |    Merupakan asal dari mana surat tersebut    |
|    file_surat_masuk    |    file    |    Merupakan file dari surat Scan/PDF   |
|    lampiran    |    int    |    Merupakan berapa halaman dari surat tersebut   |
|    keterangan_surat    |    text    |    Merupakan keterangan dari surat masuk tersebut  |
|    deposisi    |    varchar    |    Merupakan status dari surat masuk tersebut  |

#### Tabel surat_keluar

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id_surat_keluar    |    int    |    Nomor auto increment id_surat_keluar    |
|    nama_surat_keluar    |    varchar    |    Merupakan nama dari surat keluar   |
|    tanggal_keluar    |    varchar    |    Merupakan tanggal surat tersebut keluar   |
|    bulan_surat    |    varchar    |    Merupakan bulan kapan surat tersebut di rilis   |
|    file_surat_keluar    |    varchar    |    Merupakan file dari surat keluar tersebut Scan/PDF  |
|    lampiran    |    int    |    Merupakan berapa halaman dari surat tersebut    |
|    keterangan_surat    |    text    |    Merupakan keterangan dari surat keluar tersebut   |
|    klasifikasi    |    varchar    |    Merupakan jenis dari surat tersebut apakah P/R/B  |
|    tahun_surat    |    varchar    |    Merupakan tahun kapan surat tersebut keluar   |
|    nomor_surat    |    int    |    Merupakan nomor dari surat   |
|    perihal    |    varchar    |    Merupakan perihal dari surat keluar  |

#### Tabel form_survei

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id_form_survei    |    int    |    berisikan Primary Key data pada form survei    |
|    nama_survei   |    varchar    |    berisikan data nama pada survei    |
|    deskripsi   |    text    |    berisikan data deskripsi tentang survei    |
|    tanggal_dibuat   |    date    |    berisikan data tanggal kapan form di buat    |

#### Tabel pertanyaan_survei

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id_pertanyaan    |    int    |    berisikan Primary Key data pertanyaan pada survei    |
|    pertanyaan    |    varchar    |    berisikan data pertanyaan pada survei    |
|    jenis_jawaban    |    varchar    |    berisikan data tentang jenis jawaban dari pertanyaan apakah Blok Input atau Checklist    |

#### Tabel jawaban_survei

| Data Item | Type | Deskripsi |
| ------ | ------ | ------ |
|    id_jawaban    |    int    |    berisikan Primary Key data jawaban pada survei    |
|    jawaban   |    varchar    |    berisikan data jawaban pada survei    |

### Pembagian Kerja Kelompok
| Bagian | Handle |
| ------ | ------ |
|    Tujuan    |Bintang Puspita Dewi|
|    Lingkup    |Siti Nuraini|
|Akronim, singkatan, definisi|Siti Nuraini|
|Refrensi|Hardi Ananda|
|Overview|Bintang Puspita Dewi|
|Perspektif produk|Siti Nuraini|
|Antar MukaSistem|Hardi Ananda|
|Antar Pengguna|Hardi Ananda|
|Antar Perangkat Keras|Hardi Ananda|
|Spesifikasi kebutuhan Fungsional|Siti Nuraini|
|Spesifikasi kebutuhan Non-Fungsional|Siti Nuraini|
|Karakteristik Pengguna|Bintang Puspita Dewi|
|Batasan-Batasan|Hardi Ananda|
|Asumsi-asumsi|Bintang Puspita Dewi|
|Kebutuhan Penyeimbang|Siti Nuraini|
|Persyaratan Antarmuka Eksternal|Bintang Puspita Dewi|
|Functional Requirement|Bintang Puspita Dewi|
|Struktur Detail Kebutuhan Non-fungsional|Hardi Ananda|

### Hasil Wawancara

![Gambar](image/wawancara1.jpg?raw=true)

![Gambar](image/wawancara2.jpg?raw=true)

### Dokumentasi Wawancara

![Gambar](image/dokum.jpg?raw=true)
