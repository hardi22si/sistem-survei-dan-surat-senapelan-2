<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat_masuk extends Model
{
    protected $table = 'surat_masuk';
    protected $primarykey = 'id_surat_masuk';
    protected $fillable = ['id_surat_masuk'];
}
