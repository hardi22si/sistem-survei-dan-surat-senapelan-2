<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSenapelanModel extends Model
{
    use HasFactory;

    protected $table = 'users';
    protected $primarykey = 'id_user';
    protected $fillable = ['id_user', 'nama','password','hak_akses','kode'];

    public function bidang(){
        return $this->belongsTo(BidangModel::class, 'kode', 'kode');
    }
}
