<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class UserAkses
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $kode = null)
    {
        if(!$kode || auth()->user()->kode == $kode) {
            return $next($request);
        }
        elseif(auth()->user()->kode == 1) {
            return redirect()->route('camat');
        }
        elseif(auth()->user()->kode == 3) {
            return redirect()->route('admin');
        }
        return redirect()->route('pegawai');
    }
}
