<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SuratMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('surat_masuk')
            ->select('surat_masuk.*', 'bidang.*')
            ->leftjoin('bidang', 'surat_masuk.kode', '=', 'bidang.kode')
            ->get();

        return view('SuratMasuk.indexAdmin', compact('data'));
    }

    /**`
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Upload File
        $this->validate($request, [
            'nama_surat_masuk' => 'required',
            'asal_masuk' => 'required',
            'file_surat_masuk' => 'required|mimes:pdf|max:8192',
            'lampiran' => 'required',
            'keterangan_surat' => 'required',
        ]);

        //Upload File
        $file_surat_masuk = $request->file('file_surat_masuk');
        $file_surat_masuk->storeAs('public/FileSuratMasuk', $file_surat_masuk->hashName());

        //Default Deposisi
        $deposisiDefault = 'Belum Di Deposisi';

        //Insert Data
        DB::insert("INSERT INTO surat_masuk (
            `nama_surat_masuk`,
            `asal_masuk`,
            `file_surat_masuk`,
            `lampiran`,
            `keterangan_surat`,
            `deposisi`)
            VALUES (?, ?, ?, ?, ?, ?)",
            [
                $request->nama_surat_masuk,
                $request->asal_masuk,
                $file_surat_masuk->hashName(),
                $request->lampiran,
                $request->keterangan_surat,
                $deposisiDefault
            ]
        );

        return redirect()->route('SuratMasuk.index')->with('message-create', 'Surat Masuk Berhasil Di Tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // No One Else:D
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_surat_masuk' => 'required',
            'asal_masuk' => 'required',
            'file_surat_masuk' => 'mimes:pdf|max:8192',
            'lampiran' => 'required',
            'keterangan_surat' => 'required',
        ]);

        if($request->file('file_surat_masuk')){
            $nama_file_lama = DB::select("SELECT file_surat_masuk FROM surat_masuk WHERE id_surat_masuk = ?", [$id]);
            $path_file_surat_lama = $nama_file_lama[0]->file_surat_masuk;
            $path_file_surat_lama = 'public/FileSuratMasuk/'.$path_file_surat_lama;
            Storage::delete($path_file_surat_lama);

            $file_surat_masuk = $request->file('file_surat_masuk');
            $file_surat_masuk->storeAs('public/FileSuratMasuk', $file_surat_masuk->hashName());

            DB::update("UPDATE surat_masuk SET
                `nama_surat_masuk` = ?,
                `asal_masuk` = ?,
                `file_surat_masuk` = ?,
                `lampiran` = ?,
                `keterangan_surat` = ?
                WHERE `id_surat_masuk` = ?", [
                    $request->nama_surat_masuk,
                    $request->asal_masuk,
                    $request->file_surat_masuk->hashName(),
                    $request->lampiran,
                    $request->keterangan_surat,
                    $id
                ]
            );
        } else{
            DB::update("UPDATE surat_masuk SET
                `nama_surat_masuk` = ?,
                `asal_masuk` = ?,
                `lampiran` = ?,
                `keterangan_surat` = ?
                WHERE `id_surat_masuk` = ?", [
                    $request->nama_surat_masuk,
                    $request->asal_masuk,
                    $request->lampiran,
                    $request->keterangan_surat,
                    $id
                ]
            );
        }
        return redirect()->route('SuratMasuk.index')->with('message-update', 'Surat Masuk Berhasil Di Perbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nama_file_lama = DB::select("SELECT file_surat_masuk FROM surat_masuk WHERE id_surat_masuk = ?", [$id]);
        $path_file_surat_lama = $nama_file_lama[0]->file_surat_masuk;
        $path_file_surat_lama = 'public/FileSuratMasuk/'.$path_file_surat_lama;
        Storage::delete($path_file_surat_lama);
        DB::table('surat_masuk')->where('id_surat_masuk', $id)->delete();
        return redirect()->route('SuratMasuk.index')->with('message-destroy', 'Surat Masuk Berhasil Di Hapus!');
    }
}
