<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class LandingPageController extends Controller
{
    public function index(){
        $data = DB::select(DB::raw("SELECT * FROM bidang"));
        return view("LandingPage.index", compact("data"));
    }
    public function about(){
        $data = DB::select(DB::raw("SELECT * FROM bidang"));
        return view("LandingPage.tentangKami", compact("data"));
    }
}
