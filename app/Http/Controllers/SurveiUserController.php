<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SurveiUserController extends Controller
{
    public function surveiUser(){
        $data = DB::SELECT(DB::raw("SELECT * FROM form_survei"));
        return view('LandingPage.surveiMasyarakat', compact('data'));
    }
    public function surveiIsi($id_survei){
        $data = DB::select(DB::raw("SELECT * FROM pertanyaan_survei WHERE id_survei = $id_survei"));
        $data2 = DB::select(DB::raw("SELECT * FROM form_survei WHERE id_survei = $id_survei"));
        $data3 = DB::select(DB::raw("SELECT COUNT(*) + 1 as count FROM pertanyaan_survei WHERE id_survei = $id_survei AND jenis_jawaban = 'checklist'"));
        $count = $data3[0];
        return view("PertanyaanJawaban.indexFormMasyarakat", compact("data", "data2", "count", "id_survei"));
    }
    public function postJawabanSurvei(Request $request){
        $udin = $request->validate([
            'inputs.*.jawaban' => 'required',
            'inputs.*.pertanyaan' => 'required',
        ]);
        // dd($udin);
        foreach ($request->inputs as $key => $value) {
            DB::table('jawaban_survei')->insert([
                'jawaban' => $value['jawaban'],
                'id_pertanyaan' => $value['pertanyaan'],
            ]);
        }
        return redirect()->route('ThanksForm');
    }
    public function ThanksForm(){
        return view("PertanyaanJawaban.indexThanksForm");
    }
}
