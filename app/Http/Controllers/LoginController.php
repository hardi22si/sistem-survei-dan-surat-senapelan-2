<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index(){
        return view("LoginPage/login");
    }
    public function authenticate(Request $request){
        $request->validate([
            'email'=> 'required|email',
            'password'=> 'required',
        ], [
            'email.required'=> 'Email Wajib Di Isi!',
            'password.required'=> 'Password Wajib Di Isi!',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            if (Auth::check()) {
                if(Auth::user()->kode == 3){
                    $request->session()->regenerate();
                    return redirect()->intended('/dashboardAdmin');
                }
                elseif(Auth::user()->kode == 1){
                    $request->session()->regenerate();
                    return redirect()->intended('/dashboardCamat');
                }
                else{
                    $request->session()->regenerate();
                    return redirect()->intended('/dashboardPegawai');
                }
            }
            else {
                echo "Autentikasi gagal!";
            }
        }
        else{
            return back()->withErrors('Email Dan Password Yang Di Masukkan Tidak Sesuai')->withInput();
        }
    }
    public function logout(Request $request){
        Auth::logout();
        request()->session()->invalidate();
        request()->session()->regenerateToken();
        return redirect('/login');
    }
}
