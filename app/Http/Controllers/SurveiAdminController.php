<?php

namespace App\Http\Controllers;

use Illuminate\Database\DBAL\TimestampType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SurveiAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::SELECT(DB::raw("SELECT * FROM form_survei"));
        return view('Survei.indexSurveiAdmin', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Upload File
        $this->validate($request, [
            'nama_survei' => 'required',
            'deskripsi_survei' => 'required',
        ]);
        $tanggal = now();
        //Insert Data
        DB::insert("INSERT INTO form_survei (
            `nama_survei`,
            `deskripsi_survei`,
            `tanggal_dibuat`)
            VALUES (?, ?, ?)",
            [
                $request->nama_survei,
                $request->deskripsi_survei,
                $tanggal
            ]
        );
        $id = DB::SELECT(DB::raw("SELECT MAX(id_survei) as max_id FROM form_survei"));
        $maxId = $id[0]->max_id;
        return redirect()->route('insertPertanyaan', $maxId)->with('message-create', 'Survei Berhasil Di Tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_survei' => 'required',
            'deskripsi_survei' => 'required',
        ]);
        $tanggal = now();
        DB::update("UPDATE form_survei SET
                `nama_survei` = ?,
                `deskripsi_survei` = ?,
                `tanggal_dibuat` = ?
                WHERE `id_survei` = ?", [
                    $request->nama_survei,
                    $request->deskripsi_survei,
                    $tanggal,
                    $id
                ]
            );
            return redirect()->route('SurveiAdmin.index')->with('message-update', 'Survei Berhasil Di Perbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id_pertanyaan = DB::table('pertanyaan_survei')
            ->where('id_survei', $id)
            ->pluck('id_pertanyaan')
            ->toArray();

        DB::table('jawaban_survei')
            ->where('id_pertanyaan', $id_pertanyaan)
            ->delete();

        DB::table('pertanyaan_survei')->where('id_survei', $id)->delete();
        DB::table('form_survei')->where('id_survei', $id)->delete();
        return redirect()->route('SurveiAdmin.index')->with('message-destroy', 'Survei Berhasil Di Hapus!');
    }
    public function insertPertanyaan(Request $request){
        $request->validate([
            'inputs.*.pertanyaan' => 'required',
            'inputs.*.jenisjawaban' => 'required',
            'id_survei' => 'required',
        ]);
        foreach ($request->inputs as $key => $value) {
            DB::table('pertanyaan_survei')->insert([
                'pertanyaan' => $value['pertanyaan'],
                'jenis_jawaban' => $value['jenisjawaban'],
                'id_survei' => $request->id_survei,
            ]);
        }
        return redirect()->route('detailSurvei', $request->id_survei)->with('message-create', 'Pertanyaan Berhasil Di Tambahkan!');
    }

    public function detailSurvei($id)
    {
        $data = DB::SELECT(DB::raw("SELECT * FROM pertanyaan_survei WHERE id_survei = $id"));
        return view('PertanyaanJawaban.indexFormAdmin', compact('id', 'data'));
    }
    public function getInsertPertanyaan($id){
        return view('PertanyaanJawaban.indexFormCreateAdmin', compact('id'));
    }
    public function hapusPertanyaan($id_pertanyaan, $id){
        DB::table('jawaban_survei')->where('id_pertanyaan', $id_pertanyaan)->delete();
        DB::table('pertanyaan_survei')->where('id_pertanyaan', $id_pertanyaan)->delete();
        return redirect()->route('detailSurvei', $id)->with('message-destroy', 'Pertanyaan Berhasil Di Hapus!');
    }
    public function editPertanyaan(Request $request, $id_pertanyaan, $id){
        $this->validate($request, [
            'pertanyaan' => 'required',
            'jenis_jawaban' => 'required',
        ]);
        if($request->jenis_jawaban == null){
            $jenis_jawaban = null;
            DB::update("UPDATE pertanyaan_survei SET
                `pertanyaan` = ?,
                `jenis_jawaban` = ?
                WHERE `id_pertanyaan` = ?", [
                    $request->pertanyaan,
                    $jenis_jawaban,
                    $id_pertanyaan
                ]
            );
        }
        else{
            DB::update("UPDATE pertanyaan_survei SET
                `pertanyaan` = ?,
                `jenis_jawaban` = ?
                WHERE `id_pertanyaan` = ?", [
                    $request->pertanyaan,
                    $request->jenis_jawaban,
                    $id_pertanyaan
                ]
            );
        }
        // DB::update("UPDATE pertanyaan_survei SET
        //         `pertanyaan` = ?,
        //         `jenis_jawaban` = ?
        //         WHERE `id_pertanyaan` = ?", [
        //             $request->pertanyaan,
        //             $request->jenis_jawaban,
        //             $id_pertanyaan
        //         ]
        //     );
        return redirect()->route('detailSurvei', $id)->with('message-update', 'Pertanyaan Berhasil Di Perbarui!');
    }
    public function dataJawabanSurvei($id_pertanyaan, $id_survei){
        $data = DB::select(DB::raw("SELECT * FROM jawaban_survei WHERE id_pertanyaan = $id_pertanyaan"));
        // dd($data);
        return view('PertanyaanJawaban.indexJawabanSurveiAdmin', compact('data', 'id_survei'));
    }
}
