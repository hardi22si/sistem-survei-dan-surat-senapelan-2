<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SurveiCamatController extends Controller
{
    public function index(){
        $data = DB::SELECT(DB::raw("SELECT * FROM form_survei"));
        return view('Survei.indexSurveiCamat', compact('data'));
    }
    public function detailSurvei($id){
        $data = DB::SELECT(DB::raw("SELECT * FROM pertanyaan_survei WHERE id_survei = $id"));
        return view('PertanyaanJawaban.indexFormCamat', compact('id', 'data'));
    }
    public function dataJawabanSurvei($id_pertanyaan, $id_survei){
        $data = DB::select(DB::raw("SELECT * FROM jawaban_survei WHERE id_pertanyaan = $id_pertanyaan"));
        return view('PertanyaanJawaban.indexJawabanSurveiCamat', compact('data', 'id_survei'));
    }
}
