<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function admin(){
        $data = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratMasuk FROM surat_masuk"));
        $data2 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratKeluar FROM surat_keluar"));
        $data3 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as user FROM users"));
        $data4 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as bidang FROM bidang"));
        return view("Dashboard.all", compact("data", "data2", "data3", "data4"));
    }
    public function camat(){
        $data = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratMasuk FROM surat_masuk"));
        $data2 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratKeluar FROM surat_keluar"));
        $data3 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as user FROM users"));
        $data4 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as bidang FROM bidang"));
        return view("Dashboard.all", compact("data", "data2", "data3", "data4"));
    }
    public function pegawai(){
        $data = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratMasuk FROM surat_masuk"));
        $data2 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as suratKeluar FROM surat_keluar"));
        $data3 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as user FROM users"));
        $data4 = DB::SELECT(DB::raw("SELECT CONCAT(COUNT(*), '.0') as bidang FROM bidang"));
        return view("Dashboard.all", compact("data", "data2", "data3", "data4"));
    }
}
?>
