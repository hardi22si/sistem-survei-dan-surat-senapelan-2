<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SuratKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('surat_keluar')
            ->select('surat_keluar.*', 'bidang.*')
            ->leftjoin('bidang', 'surat_keluar.kode', '=', 'bidang.kode')
            ->get();
        $jumlah = DB::select(DB::raw("SELECT COUNT(*) as total FROM surat_keluar"));
        $data2 = DB::select(DB::raw("SELECT * FROM bidang"));
        return view('SuratKeluar.indexAdmin', compact('data', 'jumlah', 'data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Upload File
        $this->validate($request, [
            'nama_surat_keluar' => 'required',
            'nomor_surat' => 'required',
            'perihal' => 'required',
            'bulan_surat' => 'required',
            'tahun_surat' => 'required',
            'klasifikasi' => 'required',
            'lampiran' => 'required',
            'file_surat_keluar' => 'required|mimes:pdf|max:8192',
            'keterangan_surat' => 'required',
            'kode' => 'required',
        ]);

        //Upload File
        $file_surat_keluar = $request->file('file_surat_keluar');
        $file_surat_keluar->storeAs('public/FileSuratKeluar', $file_surat_keluar->hashName());

        //Insert Data
        DB::insert("INSERT INTO surat_keluar (
            `nama_surat_keluar`,
            `nomor_surat`,
            `perihal`,
            `bulan_surat`,
            `tahun_surat`,
            `klasifikasi`,
            `lampiran`,
            `file_surat_keluar`,
            `keterangan_surat`,
            `kode`)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $request->nama_surat_keluar,
                $request->nomor_surat,
                $request->perihal,
                $request->bulan_surat,
                $request->tahun_surat,
                $request->klasifikasi,
                $request->lampiran,
                $file_surat_keluar->hashName(),
                $request->keterangan_surat,
                $request->kode
            ]
        );
        return redirect()->route('SuratKeluar.index')->with('message-create', 'Surat Keluar Berhasil Di Tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_surat_keluar' => 'required',
            'nomor_surat' => 'required',
            'perihal' => 'required',
            'bulan_surat' => 'required',
            'tahun_surat' => 'required',
            'klasifikasi' => 'required',
            'lampiran' => 'required',
            'file_surat_keluar' => 'mimes:pdf|max:8192',
            'keterangan_surat' => 'required',
            'kode' => 'required',
        ]);

        if($request->file('file_surat_keluar')){
            $nama_file_lama = DB::select("SELECT file_surat_keluar FROM surat_keluar WHERE id_surat_keluar = ?", [$id]);
            $path_file_surat_lama = $nama_file_lama[0]->file_surat_keluar;
            $path_file_surat_lama = 'public/FileSuratKeluar/'.$path_file_surat_lama;
            Storage::delete($path_file_surat_lama);

            $file_surat_keluar = $request->file('file_surat_keluar');
            $file_surat_keluar->storeAs('public/FileSuratKeluar', $file_surat_keluar->hashName());

            DB::update("UPDATE surat_keluar SET
                `nama_surat_keluar` = ?,
                `nomor_surat` = ?,
                `perihal` = ?,
                `bulan_surat` = ?,
                `tahun_surat` = ?,
                `klasifikasi` = ?,
                `lampiran` = ?,
                `file_surat_keluar` = ?,
                `keterangan_surat` = ?,
                `kode` = ?
                WHERE `id_surat_keluar` = ?", [
                    $request->nama_surat_keluar,
                    $request->nomor_surat,
                    $request->perihal,
                    $request->bulan_surat,
                    $request->tahun_surat,
                    $request->klasifikasi,
                    $request->lampiran,
                    $file_surat_keluar->hashName(),
                    $request->keterangan_surat,
                    $request->kode,
                    $id
                ]
            );
        } else{
            DB::update("UPDATE surat_keluar SET
                `nama_surat_keluar` = ?,
                `nomor_surat` = ?,
                `perihal` = ?,
                `bulan_surat` = ?,
                `tahun_surat` = ?,
                `klasifikasi` = ?,
                `lampiran` = ?,
                `keterangan_surat` = ?,
                `kode` = ?
                WHERE `id_surat_keluar` = ?", [
                    $request->nama_surat_keluar,
                    $request->nomor_surat,
                    $request->perihal,
                    $request->bulan_surat,
                    $request->tahun_surat,
                    $request->klasifikasi,
                    $request->lampiran,
                    $request->keterangan_surat,
                    $request->kode,
                    $id
                ]
            );
        }
        return redirect()->route('SuratKeluar.index')->with('message-update', 'Surat Masuk Berhasil Di Perbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nama_file_lama = DB::select("SELECT file_surat_keluar FROM surat_keluar WHERE id_surat_keluar = ?", [$id]);
        $path_file_surat_lama = $nama_file_lama[0]->file_surat_keluar;
        $path_file_surat_lama = 'public/FileSuratKeluar/'.$path_file_surat_lama;
        Storage::delete($path_file_surat_lama);
        DB::table('surat_keluar')->where('id_surat_keluar', $id)->delete();
        return redirect()->route('SuratKeluar.index')->with('message-destroy', 'Surat Masuk Berhasil Di Hapus!');
    }
}
