<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SuratKeluarCamatController extends Controller
{
    public function index(){
        $data = DB::table('surat_keluar')
            ->select('surat_keluar.*', 'bidang.*')
            ->leftjoin('bidang', 'surat_keluar.kode', '=', 'bidang.kode')
            ->get();
        return view('SuratKeluar.indexCamat', compact('data'));
    }
}
