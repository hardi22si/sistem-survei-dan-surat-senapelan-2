<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSenapelanModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
class UserSenapelanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = UserSenapelanModel::with('bidang')->paginate(10);
        $data2 = DB::SELECT(DB::raw("SELECT * FROM bidang"));
        return view('UserSenapelan.indexAdmin', compact('data', 'data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('UserSenapelan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required',
            'password' => 'required',
            'bidang' => 'required',
        ]);
        $pass = bcrypt($request->password);
        $now = now();
        //Insert Data
        DB::insert("INSERT INTO users (
            `name`,
            `email`,
            `email_verified_at`,
            `password`,
            `remember_token`,
            `kode`,
            `created_at`,
            `updated_at`)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
            [
                $request->nama,
                $request->email,
                $now,
                $pass,
                Str::random(10),
                $request->bidang,
                $now,
                $now
            ]
        );

        return redirect()->route('UserSenapelan.index')->with('message-create', 'Pegawai Berhasil Di Tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->id == $id){
            return redirect()->route('UserSenapelan.index')->with('message-danger', 'Bidang Gagal Di Perbarui!');
        }
        else{
            $this->validate($request, [
                'nama' => 'required',
                'bidang' => 'required',
            ]);

            DB::update("UPDATE users SET
                `name` = ?,
                `kode` = ?
                WHERE `id` = ?", [
                    $request->nama,
                    $request->bidang,
                    $id
                ]
            );
            return redirect()->route('UserSenapelan.index')->with('message-update', 'Pegawai Berhasil Di Perbarui!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('UserSenapelan.index')->with('message-destroy', 'Pegawai Berhasil Di Hapus!');
    }
}
