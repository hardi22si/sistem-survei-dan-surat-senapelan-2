<?php

namespace App\Http\Controllers;

use App\Models\Surat_masuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SuratMasukCamatController extends Controller
{
    public function index(){
        $data = DB::table('surat_masuk')
            ->select('surat_masuk.*', 'bidang.*')
            ->leftjoin('bidang', 'surat_masuk.kode', '=', 'bidang.kode')
            ->get();
        $data2 = DB::SELECT(DB::raw("SELECT * FROM bidang"));
        return view('SuratMasuk.indexCamat', compact('data', 'data2'));
    }

    public function deposisi(Request $request, $id){
        $request->validate([
            'deposisi' => 'required',
            'bidang' => 'required',
        ],
        [
            'deposisi.required'=> 'Deposisi Wajib Di Isi!',
            'bidang.required'=> 'Bidang Wajib Di Isi!',
        ]);
        if($request->bidang == "null"){
            $bidang = null;
            DB::update("UPDATE surat_masuk SET
                `deposisi` = ?,
                `kode` = ?
                WHERE `id_surat_masuk` = ?", [
                    $request->deposisi,
                    $bidang,
                    $id
                ]
            );
        }
        else{
            DB::update("UPDATE surat_masuk SET
                `deposisi` = ?,
                `kode` = ?
                WHERE `id_surat_masuk` = ?", [
                    $request->deposisi,
                    $request->bidang,
                    $id
                ]
            );
        }
        return redirect()->route('SuratMasukCamat')->with('message-create', 'Deposisi Bidang Berhasil Di Ubah!');
    }
}
