<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SuratMasukPegawaiController extends Controller
{
    public function index(){
        $data = DB::table('surat_masuk')
            ->select('surat_masuk.*', 'bidang.*')
            ->where('surat_masuk.deposisi','=','Sudah Di Deposisi')
            ->leftjoin('bidang', 'surat_masuk.kode', '=', 'bidang.kode')
            ->get();
        return view('SuratMasuk.indexPegawai', compact('data'));
    }
}
