<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::SELECT(DB::raw("SELECT * FROM bidang"));
        return view('Bidang.indexAdmin', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Bidang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'kode' => 'required',
            'bidang' => 'required',
            'deskripsi' => 'required',
        ]);
        //Insert Data
        DB::insert("INSERT INTO bidang (
            `kode`,
            `bidang`,
            `deskripsi`)
            VALUES (?, ?, ?)",
            [
                $request->kode,
                $request->bidang,
                $request->deskripsi,
            ]
        );

        return redirect()->route('Bidang.index')->with('message-create', 'Bidang Berhasil Di Tambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'bidang' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::update("UPDATE bidang SET
            `bidang` = ?,
            `deskripsi` = ?
            WHERE `kode` = ?", [
                $request->bidang,
                $request->deskripsi,
                $id
            ]
        );

        return redirect()->route('Bidang.index')->with('message-update', 'Bidang Berhasil Di Perbarui!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('bidang')->where('kode', $id)->delete();
        return redirect()->route('Bidang.index')->with('message-destroy', 'Bidang Berhasil Di Hapus!');
    }
}
