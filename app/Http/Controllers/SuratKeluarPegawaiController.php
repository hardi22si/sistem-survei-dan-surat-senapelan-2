<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class SuratKeluarPegawaiController extends Controller
{
    public function index(){
        $data = DB::table('surat_keluar')
            ->select('surat_keluar.*', 'bidang.*')
            ->leftjoin('bidang', 'surat_keluar.kode', '=', 'bidang.kode')
            ->get();
        return view('SuratKeluar.indexPegawai', compact('data'));
    }
}
