@include('HeaderFooter.headerLanding')
    <!-- Carousel Start -->
    <div class="container-fluid p-0 mb-5 wow fadeIn" data-wow-delay="0.1s">
      <div id="header-carousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="w-100" src="{{ asset('landing/img/carousel-1.jpg') }}" alt="Image" />
            <div class="carousel-caption">
              <div class="container">
                <div class="row justify-content-center">
                  <div class="col-12 col-lg-10">
                    <h5
                      class="text-light text-uppercase mb-3 animated slideInDown"
                    >
                      Welcome to Kecamatan Senapelan
                    </h5>
                    <h1 class="display-2 text-light mb-3 animated slideInDown">
                      SURVEI KECAMATAN SENAPELAN
                    </h1>
                    <ol class="breadcrumb mb-4 pb-2">
                      <li class="breadcrumb-item fs-5 text-light">
                        E-Goverment
                      </li>
                      <li class="breadcrumb-item fs-5 text-light">
                        Pekanbaru
                      </li>
                      <li class="breadcrumb-item fs-5 text-light">
                        Riau
                      </li>
                    </ol>
                    <a href="" class="btn btn-primary py-3 px-5" style="background-color: #007bff; border:#007bff"
                      >Isi Survei</a
                    >
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="w-100" src="{{ asset('landing/img/carousel-2.jpg') }}" alt="Image" />
            <div class="carousel-caption">
              <div class="container">
                <div class="row justify-content-center">
                  <div class="col-12 col-lg-10">
                    <h5
                      class="text-light text-uppercase mb-3 animated slideInDown"
                    >
                      Welcome to Kecamatan Senapelan
                    </h5>
                    <h1 class="display-2 text-light mb-3 animated slideInDown">
                      Sistem Pengelolaan Data Surat Masuk Dan Keluar
                    </h1>
                    <ol class="breadcrumb mb-4 pb-2">
                        <li class="breadcrumb-item fs-5 text-light">
                          E-Goverment
                        </li>
                        <li class="breadcrumb-item fs-5 text-light">
                          Pekanbaru
                        </li>
                        <li class="breadcrumb-item fs-5 text-light">
                          Riau
                        </li>
                      </ol>
                      @if(Auth::check())
                    <a href="{{ route('login') }}" class="btn btn-primary py-3 px-5" style="background-color: #007bff; border:#007bff">
                        Surat Masuk Dan Keluar
                    </a>
                        @else
                        <a href="{{ route('login') }}" class="btn btn-primary py-3 px-5" style="background-color: #007bff; border:#007bff">
                            login
                        </a>
                        @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <button
          class="carousel-control-prev"
          type="button"
          data-bs-target="#header-carousel"
          data-bs-slide="prev"
        >
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button
          class="carousel-control-next"
          type="button"
          data-bs-target="#header-carousel"
          data-bs-slide="next"
        >
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
    <!-- Carousel End -->

    <!-- About Start -->
    <div class="container-xxl py-5">
      <div class="container">
        <div class="row g-5">
          <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
            <div
              class="position-relative overflow-hidden ps-5 pt-5 h-100"
              style="min-height: 400px"
            >
              <img
                class="position-absolute w-100 h-100"
                src="{{ asset('landing/img/about.jpg') }}"
                alt=""
                style="object-fit: cover"
              />
              <div
                class="position-absolute top-0 start-0 bg-white pe-3 pb-3"
                style="width: 200px; height: 200px" style="background-color: #007bff; border:#007bff"
              >
                <div
                  class="d-flex flex-column justify-content-center text-center h-100 p-3" style="background-color: #007bff; border:#007bff"
                >
                  <h1 class="text-white">25</h1>
                  <h2 class="text-white">Tahun</h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
            <div class="h-100">
              <div class="border-start border-5 border-primary ps-4 mb-5">
                <h6 class="text-body text-uppercase mb-2">Tentang Kami</h6>
                <h1 class="display-6 mb-0">
                  Sistem Survei, Pengelolaan Data Surat Masuk Dan Keluar Kecamatan Senapelan
                </h1>
              </div>
              <p>
                Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit,
                sed stet lorem sit clita duo justo magna dolore erat amet
              </p>
              <p class="mb-4">
                Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit,
                sed stet lorem sit clita duo justo magna dolore erat amet
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- About End -->
    <!-- Service Start -->
    <div class="container-xxl py-5">
      <div class="container">
        <div class="row g-5 align-items-end mb-5">
          <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="border-start border-5 border-primary ps-4">
              <h6 class="text-body text-uppercase mb-2">Tentang Bidang Senapelan</h6>
              <h1 class="display-6 mb-0">
                Bidang-Bidang Di Kecamatan Senapelan
              </h1>
            </div>
          </div>
        </div>
        <div class="row g-4 justify-content-center">
            @forelse($data as $bidang)
          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item bg-light overflow-hidden h-100">
              <img class="img-fluid" src="{{ asset('landing/img/service-1.jpg') }}" alt="" />
              <div class="service-text position-relative text-center h-100 p-4">
                <h5 class="mb-3">{{ $bidang->bidang }}</h5>
                <p>
                  {{ $bidang->deskripsi }}
                </p>
              </div>
            </div>
          </div>
          @empty
          <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="service-item bg-light overflow-hidden h-100">
              <img class="img-fluid" src="{{ asset('landing/img/service-1.jpg') }}" alt="" />
              <div class="service-text position-relative text-center h-100 p-4">
                <h5 class="mb-3">Data Tidak Ada</h5>
              </div>
            </div>
          </div>
          @endforelse
        </div>
      </div>
    </div>
    <!-- Service End -->
@include('HeaderFooter.footerLanding')
