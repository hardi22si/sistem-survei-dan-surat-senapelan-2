@include('HeaderFooter.headerLanding')
    <!-- Page Header Start -->
    <div
      class="container-fluid page-header py-5 mb-5 wow fadeIn"
      data-wow-delay="0.1s"
    >
      <div class="container text-center py-5">
        <h1 class="display-4 text-white animated slideInDown mb-4">Tentang Kami</h1>
      </div>
    </div>
    <!-- Page Header End -->

    <!-- About Start -->
    <div class="container-xxl py-5">
        <div class="container">
          <div class="row g-5">
            <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
              <div
                class="position-relative overflow-hidden ps-5 pt-5 h-100"
                style="min-height: 400px"
              >
                <img
                  class="position-absolute w-100 h-100"
                  src="{{ asset('landing/img/about.jpg') }}"
                  alt=""
                  style="object-fit: cover"
                />
                <div
                  class="position-absolute top-0 start-0 bg-white pe-3 pb-3"
                  style="width: 200px; height: 200px" style="background-color: #007bff; border:#007bff"
                >
                  <div
                    class="d-flex flex-column justify-content-center text-center h-100 p-3" style="background-color: #007bff; border:#007bff"
                  >
                    <h1 class="text-white">25</h1>
                    <h2 class="text-white">Tahun</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
              <div class="h-100">
                <div class="border-start border-5 border-primary ps-4 mb-5">
                  <h6 class="text-body text-uppercase mb-2">Tentang Kami</h6>
                  <h1 class="display-6 mb-0">
                    Sistem Survei, Pengelolaan Data Surat Masuk Dan Keluar Kecamatan Senapelan
                  </h1>
                </div>
                <p>
                  Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                  Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit,
                  sed stet lorem sit clita duo justo magna dolore erat amet
                </p>
                <p class="mb-4">
                  Tempor erat elitr rebum at clita. Diam dolor diam ipsum sit.
                  Aliqu diam amet diam et eos. Clita erat ipsum et lorem et sit,
                  sed stet lorem sit clita duo justo magna dolore erat amet
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- About End -->
      <!-- Service Start -->
      <div class="container-xxl py-5">
        <div class="container">
          <div class="row g-5 align-items-end mb-5">
            <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
              <div class="border-start border-5 border-primary ps-4">
                <h6 class="text-body text-uppercase mb-2">Tentang Bidang Senapelan</h6>
                <h1 class="display-6 mb-0">
                  Struktur Organisasi Kecamatan Senapelan
                </h1>
              </div>
            </div>
          </div>
          <div class="row g-4 justify-content-center">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-1.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Camat</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-2.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Sekretaris Camat</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-4.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Kasubag Umum</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-5.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">PPM</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-6.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Tantrib</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-6.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Pemerintahan</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-6.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Kesos</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.5s">
              <div class="service-item bg-light overflow-hidden h-100">
                <img class="img-fluid" src="{{ asset('landing/img/service-6.jpg') }}" alt="" />
                <div class="service-text position-relative text-center h-100 p-4">
                  <h5 class="mb-3">Paten</h5>
                  <p>
                    Tempor erat elitr rebum at clita dolor diam ipsum sit diam
                    amet diam et eos
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Service End -->
@include('HeaderFooter.footerLanding')
