<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Cannot Access Page</title>
        <meta content="Responsive admin theme build on top of Bootstrap 4" name="description" />
        <meta content="Themesdesign" name="author" />
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">

    </head>

    <body>

        <!-- Begin page -->
        <div class="error-bg"></div>
        <div class="account-pages">

                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-8">
                            <div class="card shadow-lg">
                                <div class="card-block">
                                    <div class="text-center p-3">
                                            <h1 class="error-page mt-4"><span>404!</span></h1>
                                        <h4 class="mb-4 mt-5">Maaf Anda Tidak Dapat Mengakses Halaman Ini:(</h4>
                                        {{-- <p class="mb-4">It will be as simple as Occidental in fact, it will Occidental <br> to an English person</p> --}}
                                        <a class="btn btn-primary mb-4 waves-effect waves-light" href="{{ route('landingPage') }}"><i class="mdi mdi-home"></i>Kembali Ke Dashboard</a>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- end row -->
                </div>
            </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/waves.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.js') }}"></script>

    </body>

</html>
