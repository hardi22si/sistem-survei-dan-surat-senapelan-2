@include('HeaderFooter.header')
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Data Surat Masuk</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Senapelan</li>
                            <li class="breadcrumb-item active">Data Surat Masuk</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div>
            <!-- end page-title -->

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%; margin-top: 10px; justify-content:center; align-items:center;">
                                @if(session('message-create'))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{ session('message-create') }}</strong>
                                        </div>
                                    @elseif(session('message-update'))
                                        <div class="alert alert-primary" role="alert">
                                            <strong>{{ session('message-update') }}</strong>
                                        </div>
                                    @elseif(session('message-destroy'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ session('message-destroy') }}</strong>
                                        </div>
                                    @endif
                                <a class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#create" style="float: right; margin-right: 1rem; color:white;">TAMBAH SURAT MASUK</a>
                                <thead>
                                    <tr>
                                        <th>Nama Surat</th>
                                        <th>Tanggal Masuk</th>
                                        <th>Asal</th>
                                        <th>Nomor</th>
                                        <th>Lampiran</th>
                                        <th>Perihal</th>
                                        <th>Klasifikasi</th>
                                        <th>Keterangan Surat</th>
                                        <th>File Surat Masuk</th>
                                        <th>Deposisi</th>
                                        <th>Bidang</th>
                                        <th>Edit</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $surat_masuk)
                                    <tr>
                                        <td>{{ $surat_masuk->nama_surat_masuk }}</td>
                                        <td>{{ $surat_masuk->tanggal_masuk }}</td>
                                        <td>{{ $surat_masuk->asal_masuk }}</td>
                                        <td>{{ $surat_masuk->nomor_surat }}/{{ $surat_masuk->kode }}/{{ $surat_masuk->bulan_surat }}/{{ $surat_masuk->tahun_surat }}</td>
                                        <td>{{ $surat_masuk->lampiran }}</td>
                                        <td>{{ $surat_masuk->perihal }}</td>
                                        <td>{{ $surat_masuk->klasifikasi }}</td>
                                        <td>{{ $surat_masuk->keterangan_surat }}</td>
                                        <td><u><a href="{{ Storage::url('public/FileSuratMasuk/' . $surat_masuk->file_surat_masuk) }}" target="_blank">{{ $surat_masuk->nama_surat_masuk }}.pdf</a></u></td>
                                        @if($surat_masuk->deposisi == "Sudah Di Deposisi")
                                        <td>
                                            <h5><span class="badge badge-success">{{ $surat_masuk->deposisi }}</span></h5>
                                        </td>
                                        @elseif($surat_masuk->deposisi == "Belum Di Deposisi")
                                        <td>
                                            <h5><span class="badge badge-danger">{{ $surat_masuk->deposisi }}</span></h5>
                                        </td>
                                        @else
                                        <td>
                                            <h5><span class="badge badge-primary">{{ $surat_masuk->deposisi }}</span></h5>
                                        </td>
                                        @endif
                                        @if($surat_masuk->kode == null)
                                            <td>Surat Belum Di Deposisi</td>
                                        @else
                                            <td>{{ $surat_masuk->bidang }}</td>
                                        @endif
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#edit{{ $surat_masuk->id_surat_masuk }}">Edit</a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#hapus{{ $surat_masuk->id_surat_masuk }}">Hapus</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @include('SuratMasuk.editAdmin')
                                    @include('SuratMasuk.createAdmin')
                                    @include('SuratMasuk.deleteAdmin')
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            <div class="alert alert-danger">Data formulir belum Tersedia.</div>
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- content -->
    @include('HeaderFooter.footer')
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->

<script>
    $(document).ready(function () {
        $('#suratMasukForm').submit(function (e) {
            e.preventDefault();

            // Lakukan pengiriman formulir dengan AJAX atau biarkan formulir mengirimkan data
            // Pastikan untuk menyesuaikan URL dan metode formulir sesuai dengan kebutuhan aplikasi Anda.

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (response) {
                    // Tampilkan pesan alert setelah formulir berhasil dikirim
                    showSuccessAlert();

                    // Tutup modal
                    $('#create').modal('hide');
                },
                error: function (error) {
                    // Handle kesalahan jika diperlukan
                    console.log(error);
                }
            });
        });

        function showSuccessAlert() {
            // Tampilkan pesan alert menggunakan Bootstrap classes
            var successAlert = '<div class="alert alert-primary" role="alert">' +
                '<strong>Berhasil!</strong> Anda berhasil mengirim formulir.' +
                '</div>';
            $('#alertContainer').html(successAlert);
        }
    });
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/waves.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}plugins/datatables/jszip.min.js"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('plugins/alertify/js/alertify.js') }}"></script>
<script src="{{ asset('assets/pages/alertify-init.js') }}"></script>
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>
