<!-- START MODEL TAMBAH -->
<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Surat Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="suratMasukForm" action="{{ route('SuratMasuk.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label class="font-16">Nama Surat</label>
                    <input type="text" class="form-control @error('nama_surat_masuk') is-invalid @enderror" name="nama_surat_masuk" placeholder="Nama Surat" required>
                    <label class="font-16">Asal Masuk</label>
                    <input type="text" class="form-control @error('asal_masuk') is-invalid @enderror" name="asal_masuk" placeholder="Asal Masuk" required>
                    <label class="font-16">Lampiran</label>
                    <input type="number" class="form-control @error('lampiran') is-invalid @enderror" name="lampiran" placeholder="Lampiran" required>
                    <label class="font-16">Keterangan Surat</label>
                    <textarea class="form-control @error('keterangan_surat') is-invalid @enderror" name="keterangan_surat" rows="5" placeholder="Keterangan Surat" required>{{ old('keterangan_surat') }}</textarea>
                    <label class="font-16">File Surat Masuk</label>
                    <input type="file" class="form-control @error('file_surat_masuk') is-invalid @enderror" name="file_surat_masuk" required>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
