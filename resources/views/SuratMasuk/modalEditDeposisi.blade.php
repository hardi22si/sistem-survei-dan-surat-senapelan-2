    <div class="modal fade" id="deposisi{{ $surat_masuk->id_surat_masuk }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0">Deposisi Surat Masuk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('deposisi', $surat_masuk->id_surat_masuk) }}" method="POST">
                    @csrf
                    <label class="font-15">Pilih Status Deposisi</label>
                    <select class="form-control" name="deposisi" required id="exampleDropdown">
                        <option value="">Pilih Status</option>
                        <option value="Sudah Di Deposisi">Deposisi</option>
                        <option value="Arsipkan">Arsipkan</option>
                        <option value="Belum Di Deposisi">Batal Deposisi</option>
                    </select>
                    <br>
                    <label class="font-15">Pilih Bidang Yang Ingin Di Deposisikan</label>
                    <select class="form-control" name="bidang" id="exampleDropdown">
                        <option value="null">Pilih Bidang</option>
                        @forelse ($data2 as $jabatan)
                            <option value="{{ $jabatan->kode }}">{{ $jabatan->bidang }}</option>
                        @empty
                            <option value="Data Kosong">Data Kosong</option>
                        @endforelse
                    </select>
                    <br>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">
                        Deposisi
                    </button>
                    </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
