<!-- START MODEL TAMBAH -->
<div id="edit{{ $surat_masuk->id_surat_masuk }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit {{ $surat_masuk->nama_surat_masuk }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('SuratMasuk.update', $surat_masuk->id_surat_masuk) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <label class="font-15">Nama Surat</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('nama_data') is-invalid @enderror form-control-lg" name="nama_surat_masuk" placeholder="Nama Surat" value="{{ $surat_masuk->nama_surat_masuk }}" required>
                    <label class="font-15">Asal Masuk</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('asal_masuk') is-invalid @enderror form-control-lg" name="asal_masuk" placeholder="Asal Masuk" value="{{ $surat_masuk->asal_masuk }}" required>
                    <label class="font-15">Lampiran</label>
                    <input style="margin-bottom:15px;" type="number" class="form-control @error('lampiran') is-invalid @enderror form-control-lg" name="lampiran" placeholder="Lampiran" value="{{ $surat_masuk->lampiran }}" required>
                    <label class="font-15">Keterangan Surat</label>
                    <textarea style="margin-bottom:15px;" class="form-control @error('keterangan_surat') is-invalid @enderror form-control-lg" name="keterangan_surat" rows="5" placeholder="Keterangan Surat" required>{{ $surat_masuk->keterangan_surat }}</textarea>
                    <label class="font-15">File Surat Masuk</label>
                    <input style="margin-bottom:15px;" type="file" class="form-control @error('file_data') is-invalid @enderror form-control-lg" name="file_data">
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
