<!-- START MODEL TAMBAH -->
<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Pegawai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('UserSenapelan.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="modal-body">
                        <label class="font-16">Nama Pegawai</label>
                        <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="nama" placeholder="Nama Pegawai" required>
                        <label class="font-16">Email</label>
                        <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="email" placeholder="Email" required>
                        <label class="font-16">Password</label>
                        <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="password" placeholder="Password" required>
                        <label class="font-16" for="exampleDropdown">Jabatan</label>
                        <select class="form-control" name="bidang" required id="exampleDropdown">
                            @forelse ($data2 as $jabatan)
                            <option value="{{ $jabatan->kode }}">{{ $jabatan->bidang }}</option>
                            @empty
                            <option value="Data Kosong">Data Kosong</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="alertContainer"> </div>
<!-- END MODEL TAMBAH -->
