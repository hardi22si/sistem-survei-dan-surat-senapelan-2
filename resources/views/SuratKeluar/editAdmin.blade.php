<!-- START MODEL EDIT -->
<div id="edit{{ $surat_keluar->id_surat_keluar }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit {{ $surat_keluar->nama_surat_keluar }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('SuratKeluar.update', $surat_keluar->id_surat_keluar) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <label class="font-16">Nama Surat</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('nama_data') is-invalid @enderror" name="nama_surat_keluar" placeholder="Nama Surat" value="{{ $surat_keluar->nama_surat_keluar }}" required>
                    <label class="font-16">Nomor Surat</label>
                    <input style="margin-bottom:15px;" type="number" class="form-control @error('nomor_surat') is-invalid @enderror" name="nomor_surat" placeholder="Nomor Surat" value="{{ $surat_keluar->nomor_surat }}" required>
                    <label class="font-16">Perihal</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('perihal') is-invalid @enderror" name="perihal" placeholder="Perihal" value="{{ $surat_keluar->perihal }}" required>
                    <label class="font-16">Bulan Surat</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('bulan_surat') is-invalid @enderror" name="bulan_surat" placeholder="Bulan Surat" value="{{ $surat_keluar->bulan_surat }}" required>
                    <label class="font-16">Tahun Surat</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('tahun_surat') is-invalid @enderror" name="tahun_surat" placeholder="Tahun Surat" value="{{ $surat_keluar->tahun_surat }}" required>
                    <label class="font-16">Klasifikasi</label>
                    <select style="margin-bottom:15px;" class="form-control" name="klasifikasi" required id="exampleDropdown">
                            <option value="Biasa">Biasa</option>
                            <option value="Penting">Penting</option>
                            <option value="Sangat Penting">Sangat Penting</option>
                            <option value="Rahasia">Rahasia</option>
                    </select>
                    <label class="font-16">Lampiran</label>
                    <input style="margin-bottom:15px;" type="number" class="form-control @error('lampiran') is-invalid @enderror" name="lampiran" placeholder="Lampiran" value="{{ $surat_keluar->lampiran }}" required>
                    <label class="font-16">Keterangan Surat</label>
                    <textarea style="margin-bottom:15px;" class="form-control @error('keterangan_surat') is-invalid @enderror" name="keterangan_surat" rows="5" placeholder="Keterangan Surat" required>{{ $surat_keluar->keterangan_surat }}</textarea>
                    <label class="font-16">File Surat Masuk</label>
                    <input style="margin-bottom:15px;" type="file" class="form-control @error('file_data') is-invalid @enderror" name="file_data">
                    <div class="form-group">
                        <label class="font-16">Bidang</label>
                        <select class="form-control" name="kode" required id="exampleDropdown">
                            <option value="{{ $surat_keluar->kode }}">Pilih Bidang</option>
                            @forelse($data2 as $data2)
                                <option value="{{ $data2->kode }}">{{ $data2->bidang }}</option>
                            @empty
                                <option>Data Kosong</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL EDIT -->
