<!-- START MODEL TAMBAH -->
<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Surat Keluar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('SuratKeluar.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label class="font-16">Nama Surat</label>
                    <input type="text" class="form-control @error('nama_surat_keluar') is-invalid @enderror" name="nama_surat_keluar" placeholder="Nama Surat" required>
                    <label class="font-16">Nomor Surat</label>
                    <input type="number" class="form-control @error('nomor_surat') is-invalid @enderror" name="nomor_surat" placeholder="Nomor Surat" required>
                    <label class="font-16">Perihal</label>
                    <input type="text" class="form-control @error('perihal') is-invalid @enderror" name="perihal" placeholder="Perihal" required>
                    <label class="font-16">Bulan Surat</label>
                    <input type="text" class="form-control @error('bulan_surat') is-invalid @enderror" name="bulan_surat" placeholder="Bulan Surat" required>
                    <label class="font-16">Tahun Surat</label>
                    <input type="text" class="form-control @error('tahun_surat') is-invalid @enderror" name="tahun_surat" placeholder="Tahun Surat" required>
                    <div class="form-group">
                        <label class="font-16" for="exampleDropdown">Klasifikasi</label>
                        <select class="form-control" name="klasifikasi" required id="exampleDropdown">
                            <option value="Biasa">Biasa</option>
                            <option value="Penting">Penting</option>
                            <option value="Sangat Penting">Sangat Penting</option>
                            <option value="Rahasia">Rahasia</option>
                        </select>
                    </div>
                    <label class="font-16">Lampiran</label>
                    <input type="number" class="form-control @error('lampiran') is-invalid @enderror" name="lampiran" placeholder="Lampiran" required>
                    <label class="font-16">Keterangan Surat</label>
                    <textarea class="form-control @error('keterangan_surat') is-invalid @enderror" name="keterangan_surat" rows="5" placeholder="Keterangan Surat" required>{{ old('keterangan_surat') }}</textarea>
                    <label class="font-16">File Surat Masuk</label>
                    <input type="file" class="form-control @error('file_surat_keluar') is-invalid @enderror" name="file_surat_keluar">
                    <div class="form-group">
                        <label class="font-16">Bidang</label>
                        <select class="form-control" name="kode" required id="exampleDropdown">
                            <option>Pilih Bidang</option>
                            @forelse($data2 as $data2)
                                <option value="{{ $data2->kode }}">{{ $data2->bidang }}</option>
                            @empty
                                <option>Data Kosong</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
