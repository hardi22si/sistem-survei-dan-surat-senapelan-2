@include('HeaderFooter.header')
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Data Surat Keluar</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Senapelan</a></li>
                            <li class="breadcrumb-item active">Data Surat Keluar</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div>
            <!-- end page-title -->

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%; margin-top: 10px; justify-content:center; align-items:center;">
                                @if(session('message-create'))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{ session('message-create') }}</strong>
                                        </div>
                                    @elseif(session('message-update'))
                                        <div class="alert alert-primary" role="alert">
                                            <strong>{{ session('message-update') }}</strong>
                                        </div>
                                    @elseif(session('message-destroy'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ session('message-destroy') }}</strong>
                                        </div>
                                    @endif
                                <a class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#create" style="float: right; margin-right: 1rem; color:white;">TAMBAH SURAT KELUAR</a>
                                <thead>
                                    <tr>
                                        <th>Nama Surat</th>
                                        <th>Tanggal Keluar</th>
                                        <th>Nomor</th>
                                        <th>Lampiran</th>
                                        <th>Perihal</th>
                                        <th>Klasifikasi</th>
                                        <th>Keterangan Surat</th>
                                        <th>File Surat Keluar</th>
                                        <th>Bidang</th>
                                        <th>Edit</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $surat_keluar)
                                    <tr>
                                        <td>{{ $surat_keluar->nama_surat_keluar }}</td>
                                        <td>{{ $surat_keluar->tanggal_keluar }}</td>
                                        <td>{{ $surat_keluar->nomor_surat }}/{{ $surat_keluar->kode }}/{{ $surat_keluar->bulan_surat }}/{{ $surat_keluar->tahun_surat }}</td>
                                        <td>{{ $surat_keluar->lampiran }}</td>
                                        <td>{{ $surat_keluar->perihal }}</td>
                                        <td>{{ $surat_keluar->klasifikasi }}</td>
                                        <td>{{ $surat_keluar->keterangan_surat }}</td>
                                        <td><u><a href="{{ Storage::url('public/FileSuratKeluar/' . $surat_keluar->file_surat_keluar) }}" target="_blank">{{ $surat_keluar->nama_surat_keluar }}.pdf</a></u></td>
                                        <td>{{ $surat_keluar->bidang }}</td>
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#edit{{ $surat_keluar->id_surat_keluar }}">Edit</a>
                                            </div>
                                            </td>
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#hapus{{ $surat_keluar->id_surat_keluar }}">Hapus</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @include('SuratKeluar.editAdmin')
                                    @include('SuratKeluar.createAdmin')
                                    @include('SuratKeluar.deleteAdmin')
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            <div class="alert alert-danger">Data formulir belum Tersedia.</div>
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- content -->
    @include('HeaderFooter.footer')
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->

<script>
    // Tambahkan event listener untuk menanggapi pemilihan dropdown item
    $('.dropdown-menu a').on('click', function() {
        // Ambil teks dari item yang dipilih
        var selectedText = $(this).text();

        // Perbarui teks tombol dropdown dengan teks item yang dipilih
        $('#dropdownMenuButton').text(selectedText);
    });
</script>

<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/waves.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}plugins/datatables/jszip.min.js"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.js') }}"></script>
</body>

</html>
