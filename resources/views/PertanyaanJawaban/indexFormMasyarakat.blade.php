<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="MAVIA - Register, Reservation, Questionare, Reviews form wizard">
	<meta name="author" content="Ansonika">
	<title>SURVEI SENAPELAN</title>
	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="{{ asset('survei/css/animate.min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/menu.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/icon_fonts/css/all_icons_min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/skins/square/grey.css') }}" rel="stylesheet">

	<!-- COLOR CSS -->
	<link href="{{ asset('survei/css/color_2.css') }}" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="{{ asset('survei/css/date_time_picker.css') }}" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="{{ asset('survei/css/custom.css') }}" rel="stylesheet">


    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Poppins:wght@600;700&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css"
      rel="stylesheet"
    />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('landing/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Template Stylesheet -->
    <link href="{{ asset('landing/css/style.css') }}" rel="stylesheet" />

    <style>
        body
        {
            background-color: #E8E8E8;
        }
    </style>
	<script src="js/modernizr.js"></script>
	<!-- Modernizr -->

</head>

<body>

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div><!-- /Preload -->

	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div><!-- /loader_form -->

    <nav
      class="navbar navbar-expand-lg bg-white navbar-light sticky-top px-4 px-lg-5 py-lg-0"
    >
      <a href="index.html" class="navbar-brand d-flex align-items-center">
        <h4 class="m-0">
          <i class="fa fa-building me-3"></i>SURVEI DAN SURAT KECAMATAN SENAPELAN
        </h4>
      </a>
      <button
        type="button"
        class="navbar-toggler"
        data-bs-toggle="collapse"
        data-bs-target="#navbarCollapse"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto py-3 py-lg-0">
                <a href="{{ route('landingPage') }}" class="nav-item nav-link">Dashboard</a>
                <a href="{{ route('tentangKami') }}" class="nav-item nav-link">Tentang Kami</a>
                <a href="{{ route('survei') }}" class="nav-item nav-link">Survei</a>
                @if (Auth::check())
                    <a href="{{ route('login') }}" class="nav-item nav-link">Halaman Pegawai</a>
                @else
                    <a href="{{ route('login') }}" class="nav-item nav-link">Login</a>
                @endif
        </div>
      </div>
    </nav>
	<main>
		<div id="form_container">
			<div class="row">
				<div class="col-lg-5">
					<div id="left_form">
						<figure><img src="img/registration_bg.svg" alt=""></figure>
                        @forelse($data2 as $data2)
						    <h2>{{ $data2->nama_survei }}</h2>
                            <p>{{ $data2->deskripsi_survei }}</p>
						@empty
                        <h2>Tidak Ada Data</h2>
                        @endforelse
					</div>
				</div>
				<div class="col-lg-7">
					<div id="wizard_container">
						<div id="top-wizard">
							<div id="progressbar"></div>
						</div>
						<!-- /top-wizard -->
                        @php
                            $input = 0
                        @endphp
						<form action="{{ route('surveiUserPost') }}" method="post">
                            @csrf
                            <input type="hidden" name="id_survei" value="{{ $id_survei }}">
                            <input id="website" name="website" type="text" value="">
							<!-- Leave for security protection, read docs for details -->
                            <div id="middle-wizard">
                                @php
                                    $i = 1;
                                @endphp
                                @forelse($data as $pertanyaan)
                                @if($pertanyaan->jenis_jawaban == 'blokinput')
                                    <div class="step">
                                        <h3 class="main_question"><strong>{{ $i }}/{{ $count->count }}</strong>Silahkan Masukkan Data Diri Anda</h3>
                                        @forelse($data as $pertanyaan)
                                        @if($pertanyaan->jenis_jawaban == 'blokinput')
                                        <div class="col-md-6">
											<div class="form-group">
												<input type="text" name="inputs[{{$input}}][jawaban]" class="form-control required" placeholder="{{ $pertanyaan->pertanyaan }}">
                                                <input type="hidden" name="inputs[{{$input}}][pertanyaan]" class="form-control required" value="{{ $pertanyaan->id_pertanyaan }}">
											</div>
										</div>
                                        @php
                                            $input++;
                                        @endphp
                                        @endif
                                        @empty

                                        @endforelse
                                    </div>
                                    @php
                                        $i++;
                                    @endphp
                                    @break
                                @endif
                                @empty

                                @endforelse
                                @forelse($data as $pertanyaan)
                                @if($pertanyaan->jenis_jawaban == 'checklist')
                                    @if($i == $count->count)
                                        <div class="submit step">
                                            <h3 class="main_question"><strong>{{ $i }}/{{ $count->count }}</strong>{{ $pertanyaan->pertanyaan }}</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Sangat Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Sangat Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Cukup Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Cukup Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Kurang Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Kurang Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Tidak Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Tidak Baik</label>
                                                    </div>
                                                    <input type="hidden" name="inputs[{{$input}}][pertanyaan]" class="form-control required" value="{{ $pertanyaan->id_pertanyaan }}">
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="step">
                                            <h3 class="main_question"><strong>{{ $i }}/{{ $count->count }}</strong>{{ $pertanyaan->pertanyaan }}</h3>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Sangat Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Sangat Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Cukup Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Cukup Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Kurang Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Kurang Baik</label>
                                                    </div>
                                                    <div class="form-group radio_input">
                                                        <label><input type="radio" value="Tidak Baik" name="inputs[{{$input}}][jawaban]" class="icheck required">Tidak Baik</label>
                                                    </div>
                                                    <input type="hidden" name="inputs[{{$input}}][pertanyaan]" class="form-control required" value="{{ $pertanyaan->id_pertanyaan }}">
                                                </div>
                                            </div>
                                        </div>
                                        @php
                                            $i++;
                                            $input++;
                                        @endphp
                                    @endif
                                @endif
                                @empty
                                    <h4>Data Formulir Kosong</h4>
                                @endforelse
                            </div>
							<!-- /middle-wizard -->
							<div id="bottom-wizard">
                                @if($data != null)
                                {{-- <button type="button" class="btn-danger forward">Kembali Ke Dashboard</button> --}}
								<button type="button" name="backward" class="backward">Backward </button>
                                <button type="button" name="forward" class="forward">Forward</button>
								<button type="submit" name="process" class="submit">Submit</button>
                                @endif
                            </div>
							<!-- /bottom-wizard -->
						</form>
					</div>
					<!-- /Wizard container -->
				</div>
			</div><!-- /Row -->
		</div><!-- /Form_container -->
	</main>

	<footer id="home" class="clearfix">
		<p>© 2023 Hardi Ananda</p>
		</ul>
	</footer>
	<!-- end footer-->

	<div class="cd-overlay-nav">
		<span></span>
	</div>
	<!-- cd-overlay-nav -->

	<div class="cd-overlay-content">
		<span></span>
	</div>
	<!-- cd-overlay-content -->


	<!-- SCRIPTS -->
	<!-- Jquery-->
	<script src="{{ asset('survei/js/jquery-3.2.1.min.js') }}"></script>
	<!-- Common script -->
	<script src="{{ asset('survei/js/common_scripts.js') }}"></script>
	<!-- Wizard script -->
	<script src="{{ asset('survei/js/questionare_wizard_func.js') }}"></script>
	<!-- Menu script -->
	<script src="{{ asset('survei/js/velocity.min.js') }}"></script>
	<script src="{{ asset('survei/js/main.js') }}"></script>
	<!-- Theme script -->
	<script src="{{ asset('survei/js/functions.js') }}"></script>
</body>
</html>
