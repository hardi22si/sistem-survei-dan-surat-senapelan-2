@include('HeaderFooter.header')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">Form Pertanyaan Survei</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item">Senapelan</li>
                                        <li class="breadcrumb-item">Tambah Data Pertanyaan</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title">Tambahkan Pertanyaan</h4>
                                        <form action="/InsertPertanyaanPost" method="post" enctype="multipart/form-data">
                                            @csrf
                                            <button type="submit" class="btn btn-success" style="color:white; margin-top:1rem; margin-bottom:1rem;">SIMPAN DATA</button>
                                            <a href="{{ route('detailSurvei', $id) }}" type="submit" class="btn btn-primary" style="margin-left: 1rem">KEMBALI</a>
                                            <table id="table" class="table table-striped table-bordered">
                                                <tbody>
                                                <tr>
                                                    <th>Pertanyaan</th>
                                                    <th>Jenis Jawaban</th>
                                                    <th style="text-align: center">Aksi</th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-control" type="text" name="inputs[0][pertanyaan]" placeholder="Pertanyaan" required>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="inputs[0][jenisjawaban]" required id="exampleDropdown">
                                                            <option value="checklist">Check List</option>
                                                            <option value="blokinput">Blok Input</option>
                                                        </select>
                                                    </td>
                                                    <td style="width:10rem; text-align:center">
                                                        <button type="button" name="add" id="add" class="btn btn-success waves-effect waves-light" style="color:white;">TAMBAH</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            </table>
                                            <input type="hidden" name="id_survei" value="{{ $id }}">
                                        </form>
                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->


                    </div>
                    <!-- container-fluid -->

                </div>
                <!-- content -->

                @include('HeaderFooter.footer')
            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!-- jQuery  -->
        <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('assets/js/waves.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('assets/js/app.js') }}"></script>

        <script>
            var i = 0;
            $('#add').click(function(){
                ++i;
                $('#table').append(
                    `
                    <tr>
                        <td>
                            <input class="form-control" type="text" name="inputs[`+i+`][pertanyaan]" value="" placeholder="Pertanyaan" required>
                        </td>
                        <td>
                            <select class="form-control" name="inputs[`+i+`][jenisjawaban]" required id="exampleDropdown">
                                <option value="checklist">Check List</option>
                                <option value="blokinput">Blok Input</option>
                            </select>
                        </td>
                        <td style="width:10rem; text-align:center">
                            <button type="button" class="btn btn-danger waves-effect waves-light remove-table-row" style="color:white;">HAPUS</button>
                        </td>
                    </tr>
                    `
                );
            });
            $(document).on('click', '.remove-table-row', function(){
                $(this).parents('tr').remove();
            })
        </script>
    </body>

</html>


<!-- START MODEL TAMBAH -->
<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Surat Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="suratMasukForm" action="{{ route('SuratMasuk.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label class="font-16">Nama Surat</label>
                    <input type="text" class="form-control @error('nama_surat_masuk') is-invalid @enderror" name="nama_surat_masuk" placeholder="Nama Surat" required>
                    <label class="font-16">Asal Masuk</label>
                    <input type="text" class="form-control @error('asal_masuk') is-invalid @enderror" name="asal_masuk" placeholder="Asal Masuk" required>
                    <label class="font-16">Lampiran</label>
                    <input type="number" class="form-control @error('lampiran') is-invalid @enderror" name="lampiran" placeholder="Lampiran" required>
                    <label class="font-16">Keterangan Surat</label>
                    <textarea class="form-control @error('keterangan_surat') is-invalid @enderror" name="keterangan_surat" rows="5" placeholder="Keterangan Surat" required>{{ old('keterangan_surat') }}</textarea>
                    <label class="font-16">File Surat Masuk</label>
                    <input type="file" class="form-control @error('file_surat_masuk') is-invalid @enderror" name="file_surat_masuk" required>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
