<!-- START MODEL TAMBAH -->
<div id="edit{{ $pertanyaan->id_pertanyaan }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit Pertanyaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('editPertanyaan', ['id_pertanyaan' => $pertanyaan->id_pertanyaan, 'id' => $id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <textarea style="margin-bottom:15px;" class="form-control @error('keterangan_surat') is-invalid @enderror form-control-lg" name="pertanyaan" rows="5" placeholder="Keterangan Surat" required>{{ $pertanyaan->pertanyaan }}</textarea>
                    <label class="font-15">Jenis Jawaban</label>
                    <select class="form-control" name="jenis_jawaban" id="exampleDropdown">
                        <option value="checklist">CheckList</option>
                        <option value="blokinput">Input Blok</option>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
