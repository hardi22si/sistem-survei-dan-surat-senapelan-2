@include('HeaderFooter.header')
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Data Suvei</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Senapelan</li>
                            <li class="breadcrumb-item active">Pertanyaan Survei</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div>
            <!-- end page-title -->

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%; margin-top: 10px; justify-content:center; align-items:center;">
                                <a class="btn btn-primary waves-effect waves-light" href="{{ route('SurveiMasyarakat') }}" style="float: right; margin-right: 1rem; color:white; margin-bottom:1rem">KEMBALI</a>
                                <thead>
                                    <tr>
                                        <th>Pertanyaan</th>
                                        <th>Jenis Jawaban</th>
                                        <th>Jawaban Survei</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $pertanyaan)
                                    <tr>
                                        <td>{{ $pertanyaan->pertanyaan }}</td>
                                        @if($pertanyaan->jenis_jawaban == 'checklist')
                                            <td>Check List</td>
                                        @endif
                                        @if($pertanyaan->jenis_jawaban == 'blokinput')
                                            <td>Blok Input</td>
                                        @endif
                                        <td>
                                            <div class="text-center">
                                                <a href="{{ route('dataJawabanSurveiCamat', ['id_pertanyaan' => $pertanyaan->id_pertanyaan, 'id_survei' => $id]) }}" class="btn btn-warning waves-effect waves-light">Lihat</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            <div class="alert alert-danger">Data formulir belum Tersedia.</div>
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- content -->
    @include('HeaderFooter.footer')
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->
<script>
    var i = 0;
    $('#add').click(function(){
        ++i;
        $('#table').append(
            `
            <tr>
                <td>
                    <input class="form-control" type="text" name="inputs[`+i+`][pertanyaan]" value="" placeholder="Pertanyaan" required>
                </td>
                <td style="width:10rem; text-align:center">
                    <button type="button" class="btn btn-danger waves-effect waves-light remove-table-row" style="color:white;">HAPUS</button>
                </td>
            </tr>
            `
        );
    });
    $(document).on('click', '.remove-table-row', function(){
        $(this).parents('tr').remove();
    })
</script>
<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/waves.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}plugins/datatables/jszip.min.js"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.js') }}"></script>

</body>

</html>
