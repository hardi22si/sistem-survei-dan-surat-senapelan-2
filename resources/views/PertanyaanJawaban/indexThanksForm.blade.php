<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="MAVIA - Register, Reservation, Questionare, Reviews form wizard">
	<meta name="author" content="Ansonika">
	<title>SURVEI SENAPELAN</title>
	<!-- GOOGLE WEB FONT -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="{{ asset('survei/css/animate.min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/menu.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/style.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/icon_fonts/css/all_icons_min.css') }}" rel="stylesheet">
	<link href="{{ asset('survei/css/skins/square/grey.css') }}" rel="stylesheet">

	<!-- COLOR CSS -->
	<link href="{{ asset('survei/css/color_2.css') }}" rel="stylesheet">

	<!-- BASE CSS -->
	<link href="{{ asset('survei/css/date_time_picker.css') }}" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="{{ asset('survei/css/custom.css') }}" rel="stylesheet">


    <link
      href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Poppins:wght@600;700&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css"
      rel="stylesheet"
    />

    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('landing/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Template Stylesheet -->
    <link href="{{ asset('landing/css/style.css') }}" rel="stylesheet" />

    <style>
        body
        {
            background-color: #E8E8E8;
        }
    </style>
	<script src="js/modernizr.js"></script>
	<!-- Modernizr -->

</head>

<body>

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div><!-- /Preload -->

	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div><!-- /loader_form -->

    <nav
      class="navbar navbar-expand-lg bg-white navbar-light sticky-top px-4 px-lg-5 py-lg-0"
    >
      <a href="index.html" class="navbar-brand d-flex align-items-center">
        <h4 class="m-0">
          <i class="fa fa-building me-3"></i>SURVEI DAN SURAT KECAMATAN SENAPELAN
        </h4>
      </a>
      <button
        type="button"
        class="navbar-toggler"
        data-bs-toggle="collapse"
        data-bs-target="#navbarCollapse"
      >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <div class="navbar-nav ms-auto py-3 py-lg-0">
                <a href="{{ route('landingPage') }}" class="nav-item nav-link">Dashboard</a>
                <a href="{{ route('tentangKami') }}" class="nav-item nav-link">Tentang Kami</a>
                <a href="{{ route('survei') }}" class="nav-item nav-link">Survei</a>
                @if (Auth::check())
                    <a href="{{ route('login') }}" class="nav-item nav-link">Halaman Pegawai</a>
                @else
                    <a href="{{ route('login') }}" class="nav-item nav-link">Login</a>
                @endif
        </div>
      </div>
    </nav>
	<main>
		<div id="form_container">
			<div class="row">
					<div id="left_form">
						<figure><img src="{{ asset('survei/img/registration_bg.svg') }}" alt=""></figure>
                        <h2>Terima Sudah Mengisi Survei!</h2>
                        <p><B>KECAMATAN SENAPELAN</B></p>
                        <a href="{{ route('landingPage') }}" class="btn btn-outline-light" style="width:25rem">Home</a>
                    </div>
					<!-- /Wizard container -->
				</div>
			</div><!-- /Row -->
		</div><!-- /Form_container -->
	</main>

	<footer id="home" class="clearfix">
		<p>© 2023 Hardi Ananda</p>
		</ul>
	</footer>
	<!-- end footer-->

	<div class="cd-overlay-nav">
		<span></span>
	</div>
	<!-- cd-overlay-nav -->

	<div class="cd-overlay-content">
		<span></span>
	</div>
	<!-- cd-overlay-content -->


	<!-- SCRIPTS -->
	<!-- Jquery-->
	<script src="{{ asset('survei/js/jquery-3.2.1.min.js') }}"></script>
	<!-- Common script -->
	<script src="{{ asset('survei/js/common_scripts.js') }}"></script>
	<!-- Wizard script -->
	<script src="{{ asset('survei/js/questionare_wizard_func.js') }}"></script>
	<!-- Menu script -->
	<script src="{{ asset('survei/js/velocity.min.js') }}"></script>
	<script src="{{ asset('survei/js/main.js') }}"></script>
	<!-- Theme script -->
	<script src="{{ asset('survei/js/functions.js') }}"></script>
</body>
</html>
