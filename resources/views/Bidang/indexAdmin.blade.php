@include('HeaderFooter.header')
<!-- Left Sidebar End -->

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">Data Bidang</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item">Senapelan</li>
                            <li class="breadcrumb-item active">Data Bidang</li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div>
            <!-- end page-title -->

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%; margin-top: 10px; justify-content:center; align-items:center;">
                                @if(session('message-create'))
                                        <div class="alert alert-success" role="alert">
                                            <strong>{{ session('message-create') }}</strong>
                                        </div>
                                    @elseif(session('message-update'))
                                        <div class="alert alert-primary" role="alert">
                                            <strong>{{ session('message-update') }}</strong>
                                        </div>
                                    @elseif(session('message-destroy'))
                                        <div class="alert alert-danger" role="alert">
                                            <strong>{{ session('message-destroy') }}</strong>
                                        </div>
                                    @endif
                                <a class="btn btn-success waves-effect waves-light" data-toggle="modal" data-target="#create" style="float: right; margin-right: 1rem; color:white;">TAMBAH BIDANG</a>
                                <thead>
                                    <tr>
                                        <th>Kode</th>
                                        <th>Bidang / Seksi</th>
                                        <th>Deskripsi</th>
                                        <th>Edit</th>
                                        <th>Hapus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $bidangseksi)
                                    <tr>
                                        <td>{{ $bidangseksi->kode }}</td>
                                        <td>{{ $bidangseksi->bidang }}</td>
                                        <td>{{ $bidangseksi->deskripsi }}</td>
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#edit{{ $bidangseksi->kode }}">Edit</a>
                                            </div>
                                            </td>
                                        <td>
                                            <div class="text-center">
                                                <a href="#" class="btn btn-danger waves-effect waves-light" data-toggle="modal" data-target="#hapus{{ $bidangseksi->kode }}">Hapus</a>
                                            </div>
                                        </td>
                                    </tr>
                                    @include('Bidang.editAdmin')
                                    @include('Bidang.createAdmin')
                                    @include('Bidang.deleteAdmin')
                                    @empty
                                    <tr>
                                        <td colspan="9" class="text-center">
                                            <div class="alert alert-danger">Data formulir belum Tersedia.</div>
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->
        </div>
        <!-- container-fluid -->
    </div>
    <!-- content -->
    @include('HeaderFooter.footer')
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
</div>
<!-- END wrapper -->

<!-- jQuery  -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/metismenu.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('assets/js/waves.min.js') }}"></script>

<!-- Required datatable js -->
<script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- Buttons examples -->
<script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/jszip.min.js') }}plugins/datatables/jszip.min.js"></script>
<script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
<!-- Responsive examples -->
<script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>

<!-- Datatable init js -->
<script src="{{ asset('assets/pages/datatables.init.js') }}"></script>

<!-- App js -->
<script src="{{ asset('assets/js/app.js') }}"></script>
</body>

</html>
