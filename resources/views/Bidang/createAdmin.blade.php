<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Surat Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Bidang.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label class="font-16">Kode</label>
                    <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="kode" placeholder="Kode Bidang" required>
                    <label class="font-16">Bidang</label>
                    <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="bidang" placeholder="Nama Bidang" required>
                    <label class="font-16">Deskripsi</label>
                    <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" rows="5" placeholder="Deskripsi Bidang" required>{{ old('deskripsi') }}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
