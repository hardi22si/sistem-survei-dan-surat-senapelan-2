<div id="edit{{ $bidangseksi->kode }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit Data {{ $bidangseksi->bidang }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('Bidang.update', $bidangseksi->kode) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <label class="font-16">Bidang</label>
                    <input type="text" class="form-control @error('lampiran') is-invalid @enderror" name="bidang" placeholder="Asal Masuk" value="{{ $bidangseksi->bidang }}" required>
                    <label class="font-16">Deskripsi</label>
                    <textarea class="form-control @error('deskripsi') is-invalid @enderror" name="deskripsi" rows="5" placeholder="Deskripsi Bidang" required>{{ $bidangseksi->deskripsi }}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
