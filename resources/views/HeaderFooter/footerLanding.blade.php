<!-- Footer Start -->
<div
class="container-fluid bg-dark footer mt-5 pt-5 wow fadeIn"
data-wow-delay="0.1s"
>
<div class="container py-5">
  <div class="row g-5">
    <div class="col-lg-3 col-md-6">
      <h1 class="text-white mb-4">
        <i class="fa fa-building text-primary me-3" style="background-color: #007bff; border:#007bff"></i>SENAPELAN
      </h1>
      <p>
        Diam dolor diam ipsum sit. Aliqu diam amet diam et eos. Clita erat
        ipsum et lorem et sit, sed stet lorem sit clita
      </p>
    </div>
    <div class="col-lg-3 col-md-6">
      <h4 class="text-light mb-4">Alamat</h4>
      <p>
        <i class="fa fa-map-marker-alt me-3"></i>123 Street, New York, USA
      </p>
      <p><i class="fa fa-phone-alt me-3"></i>+012 345 67890</p>
      <p><i class="fa fa-envelope me-3"></i>info@example.com</p>
    </div>
    <div class="col-lg-3 col-md-6">
      <h4 class="text-light mb-4">Link Terkait</h4>
      <a class="btn btn-link" href="">About Us</a>
      <a class="btn btn-link" href="">Login</a>
      <a class="btn btn-link" href="">Survei</a>
    </div>
    <div class="col-lg-3 col-md-6">
      <h4 class="text-light mb-4">Tentang Pengembang</h4>
      <p>Dolor amet sit justo amet elitr clita ipsum elitr est.</p>
    </div>
  </div>
</div>
<div class="container-fluid copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
        &copy; <a href="#">Hardi Ananda</a>, All Right Reserved.
      </div>
    </div>
  </div>
</div>
</div>
<!-- Footer End -->

<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"
><i class="bi bi-arrow-up"></i
></a>

<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="{{ asset('landing/lib/wow/wow.min.js') }}"></script>
<script src="{{ asset('landing/lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('landing/lib/waypoints/waypoints.min.js') }}"></script>
<script src="{{ asset('landing/lib/owlcarousel/owl.carousel.min.js') }}"></script>

<!-- Template Javascript -->
<script src="{{ asset('landing/js/main.js') }}"></script>
</body>
</html>
