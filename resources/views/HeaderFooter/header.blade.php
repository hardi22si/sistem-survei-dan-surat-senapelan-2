<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if(Auth::user()->kode == 3)
        <title>DASHBOARD ADMIN SENAPELAN</title>
        @elseif(Auth::user()->kode == 1)
        <title>DASHBOARD CAMAT SENAPELAN</title>
        @else
        <title>DASHBOARD PEGAWAI SENAPELAN</title>
        @endif
        <meta content="Responsive admin theme build on top of Bootstrap 4" name="description" />
        <meta content="Themesdesign" name="author" />
        <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

        <!-- DataTables -->
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- Responsive datatable examples -->
        <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css">
    </head>

    <body>
        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo">
                        <span class="logo-light">
                            <i class="mdi mdi-camera-control"></i>SURAT AND SURVEI
                        </span>
                        <span class="logo-sm">
                            <i class="mdi mdi-camera-control"></i>
                        </span>
                    </a>
                </div>

                <nav class="navbar-custom">
                    <ul class="navbar-right list-inline float-right mb-0">

                        <!-- full screen -->
                        <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                            <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                                <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                            </a>
                        </li>

                        <li class="dropdown notification-list list-inline-item">
                            <div class="dropdown notification-list nav-pro-img">
                                <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                    <img src="{{ asset('assets/images/users/user-4.jpg') }}" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <a class="dropdown-item" href="{{ route('landingPage') }}"><i class="mdi mdi-view-dashboard"></i>Dashboard Utama</a>
                                    <div class="dropdown-divider"></div>
                                    <form action="/logout" method="POST">
                                        @csrf
                                        <button type="submit" class="dropdown-item text-danger"><i class="mdi mdi-power text-danger"></i> Logout</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-effect">
                                <i class="mdi mdi-menu"></i>
                            </button>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Menu</li>
                            @if(Auth::user()->kode == 3)
                                <li>
                                    <a href="{{ route('admin') }}" class="waves-effect">
                                        <i class="icon-accelerator"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
                                    </a>
                                </li>
                            @elseif(Auth::user()->kode == 1)
                                <li>
                                    <a href="{{ route('camat') }}" class="waves-effect">
                                        <i class="icon-accelerator"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ route('pegawai') }}" class="waves-effect">
                                        <i class="icon-accelerator"></i><span class="badge badge-success badge-pill float-right"></span> <span> Dashboard </span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->kode == 3)
                                <li>
                                    <a href="{{ route('SuratMasuk.index') }}">
                                        <i class="mdi mdi-file-document-box-multiple-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Masuk </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('SuratKeluar.index') }}">
                                        <i class="mdi mdi-file-document-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Keluar </span>
                                    </a>
                                </li>
                            @elseif(Auth::user()->kode == 1)
                                <li>
                                    <a href="{{ route('SuratMasukCamat') }}">
                                        <i class="mdi mdi-file-document-box-multiple-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Masuk </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('SuratKeluarCamat') }}">
                                        <i class="mdi mdi-file-document-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Keluar </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('SurveiMasyarakat') }}">
                                        <i class="icon-todolist-check"></i><span class="badge badge-success badge-pill float-right"></span> <span> Survei Masyarakat </span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ route('SuratMasukPegawai') }}">
                                        <i class="mdi mdi-file-document-box-multiple-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Masuk </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('SuratKeluarPegawai') }}">
                                        <i class="mdi mdi-file-document-outline"></i><span class="badge badge-success badge-pill float-right"></span> <span> Surat Keluar </span>
                                    </a>
                                </li>
                            @endif
                            @if(Auth::user()->kode == 3)
                            <li>
                                <a href="{{ route('UserSenapelan.index') }}">
                                    <i class="mdi mdi-account-multiple"></i><span class="badge badge-success badge-pill float-right"></span> <span> Pegawai </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('Bidang.index') }}">
                                    <i class="mdi mdi-book-multiple"></i><span class="badge badge-success badge-pill float-right"></span> <span> Bidang </span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('SurveiAdmin.index') }}">
                                    <i class="icon-todolist-check"></i><span class="badge badge-success badge-pill float-right"></span> <span> Survei Masyarakat </span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
