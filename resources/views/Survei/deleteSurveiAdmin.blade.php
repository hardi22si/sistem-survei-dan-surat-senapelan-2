<!-- START MODEL TAMBAH -->
<div id="hapus{{ $survei->id_survei }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Apakah Anda Yakin Ingin Menghapus {{ $survei->nama_survei }}?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Ini Akan Menghapus Seluruh Data Pertanyaan Dan Jawaban Pada Survei Ini</p>
            </div>
            <form action="{{ route('SurveiAdmin.destroy', $survei->id_survei) }}" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
