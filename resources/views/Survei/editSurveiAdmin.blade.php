<!-- START MODEL TAMBAH -->
<div id="edit{{ $survei->id_survei }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Edit {{ $survei->nama_survei }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('SurveiAdmin.update', $survei->id_survei) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="modal-body">
                    <label class="font-15">Nama Survei</label>
                    <input style="margin-bottom:15px;" type="text" class="form-control @error('nama_data') is-invalid @enderror form-control-lg" name="nama_survei" placeholder="Nama Surat" value="{{ $survei->nama_survei }}" required>
                    <label class="font-15">Deskripsi Survei</label>
                    <textarea style="margin-bottom:15px;" class="form-control @error('keterangan_surat') is-invalid @enderror form-control-lg" name="deskripsi_survei" rows="5" placeholder="Keterangan Surat" required>{{ $survei->deskripsi_survei }}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
