<!-- START MODEL TAMBAH -->
<div id="create" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">Tambah Survei</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="suratMasukForm" action="{{ route('SurveiAdmin.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <label class="font-16">Nama Survei</label>
                    <input type="text" class="form-control @error('nama_surat_masuk') is-invalid @enderror" name="nama_survei" placeholder="Nama Survei" required>
                    <label class="font-16">Deskripsi Survei</label>
                    <textarea class="form-control @error('keterangan_surat') is-invalid @enderror" name="deskripsi_survei" rows="5" placeholder="Deskripsi Survei" required>{{ old('deskripsi_survei') }}</textarea>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary waves-effect waves-light">Reset</button>
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light" id="alertify-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END MODEL TAMBAH -->
